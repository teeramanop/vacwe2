/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package xpdlparser;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.apache.commons.digester.Digester;

/**
 *
 * @author teeramanop
 */
public class XPDLParser {
     	public Pack StartParse(String CompCode,String LicenseKey,InputStream FileName) {
            if (!verLicense(CompCode,LicenseKey)) {
                return null;
            }
            return StartParse(FileName);
        }

	public Pack StartParse(InputStream FileName) {
                Date myDate = new Date();
                System.out.println("[vacwe Info]:" + myDate +"-- Workflow Engine by Value Alliance Consulting Co.,Ltd. (Thailand) --");
		try {
			Digester digester = new Digester();
			digester.setValidating(false);
			
			digester.addObjectCreate("Package",Pack.class);
			digester.addSetProperties("Package","Id","id");
			digester.addSetProperties("Package","Name","name");
			
			digester.addObjectCreate("Package/PackageHeader",PackHeader.class);			
			digester.addBeanPropertySetter("Package/PackageHeader/Created","created");
			digester.addSetNext("Package/PackageHeader","addPackHeader");			

			digester.addObjectCreate("Package/Participants/Participant",Participant.class);			
			digester.addSetProperties("Package/Participants/Participant","Id","id");
			digester.addSetProperties("Package/Participants/Participant","Name","name");
			digester.addObjectCreate("Package/Participants/Participant/ParticipantType",ParticipantType.class);			
			digester.addSetProperties("Package/Participants/Participant/ParticipantType","Type","type");
			digester.addSetNext("Package/Participants/Participant/ParticipantType","addParticipantType");			
			digester.addSetNext("Package/Participants/Participant","addParticipant");			

			digester.addObjectCreate("Package/Applications/Application",Application.class);
			digester.addSetProperties("Package/Applications/Application","Id","id");
			digester.addSetProperties("Package/Applications/Application","Name","name");
			digester.addBeanPropertySetter("Package/Applications/Application/Description","description");
			digester.addObjectCreate("Package/Applications/Application/FormalParameters/FormalParameter",FormalParameter.class);
			digester.addSetProperties("Package/Applications/Application/FormalParameters/FormalParameter","Id","id");
			digester.addSetProperties("Package/Applications/Application/FormalParameters/FormalParameter","Index","index");
			digester.addSetProperties("Package/Applications/Application/FormalParameters/FormalParameter","Mode","mode");
			digester.addObjectCreate("Package/Applications/Application/FormalParameters/FormalParameter/DataType/BasicType",DataType.class);			
			digester.addSetProperties("Package/Applications/Application/FormalParameters/FormalParameter/DataType/BasicType","Type","type");
			digester.addSetNext("Package/Applications/Application/FormalParameters/FormalParameter/DataType/BasicType","addDataType");			
			digester.addSetNext("Package/Applications/Application/FormalParameters/FormalParameter","addFormalParameter");			
			digester.addSetNext("Package/Applications/Application","addApplication");			

			digester.addObjectCreate("Package/WorkflowProcesses/WorkflowProcess",WorkflowProcess.class);			
			digester.addSetProperties("Package/WorkflowProcesses/WorkflowProcess","Id","id");
			digester.addSetProperties("Package/WorkflowProcesses/WorkflowProcess","Name","name");
			digester.addObjectCreate("Package/WorkflowProcesses/WorkflowProcess/DataFields/DataField",DataField.class);			
			digester.addSetProperties("Package/WorkflowProcesses/WorkflowProcess/DataFields/DataField","Id","id");
			digester.addSetProperties("Package/WorkflowProcesses/WorkflowProcess/DataFields/DataField","IsArray","isArray");
			digester.addSetProperties("Package/WorkflowProcesses/WorkflowProcess/DataFields/DataField","Name","name");
			digester.addBeanPropertySetter("Package/WorkflowProcesses/WorkflowProcess/DataFields/DataField/Description","description");
			digester.addObjectCreate("Package/WorkflowProcesses/WorkflowProcess/DataFields/DataField/DataType/BasicType",DataType.class);			
			digester.addSetProperties("Package/WorkflowProcesses/WorkflowProcess/DataFields/DataField/DataType/BasicType","Type","type");
			digester.addSetNext("Package/WorkflowProcesses/WorkflowProcess/DataFields/DataField/DataType/BasicType","addDataType");			
			digester.addSetNext("Package/WorkflowProcesses/WorkflowProcess/DataFields/DataField","addDataField");			

			digester.addObjectCreate("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity",Activity.class);			
			digester.addSetProperties("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity","Id","id");
			digester.addSetProperties("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity","Name","name");
			digester.addBeanPropertySetter("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity/Description","description");
			digester.addBeanPropertySetter("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity/Performer","performer");
			digester.addBeanPropertySetter("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity/Priority","priority");
			digester.addBeanPropertySetter("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity/Icon","icon");
			digester.addObjectCreate("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity/Implementation/SubFlow",Subflow.class);			
			digester.addSetProperties("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity/Implementation/SubFlow","Execution","execution");
			digester.addSetProperties("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity/Implementation/SubFlow","Id","id");
			digester.addSetNext("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity/Implementation/SubFlow","addSubflow");
						
			digester.addObjectCreate("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity/Implementation/Tool",Tool.class);			
			digester.addSetProperties("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity/Implementation/Tool","Id","id");
			digester.addSetProperties("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity/Implementation/Tool","Type","type");
			digester.addObjectCreate("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity/Implementation/Tool/ActualParameters/ActualParameter",ActualParameter.class);			
			digester.addBeanPropertySetter("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity/Implementation/Tool/ActualParameters/ActualParameter","parameter");
			digester.addSetNext("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity/Implementation/Tool/ActualParameters/ActualParameter","addActualParameter");			
			digester.addSetNext("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity/Implementation/Tool","addTool");			
			digester.addSetNext("Package/WorkflowProcesses/WorkflowProcess/Activities/Activity","addActivity");			

			digester.addObjectCreate("Package/WorkflowProcesses/WorkflowProcess/Transitions/Transition",Transition.class);			
			digester.addSetProperties("Package/WorkflowProcesses/WorkflowProcess/Transitions/Transition","Id","id");
			digester.addSetProperties("Package/WorkflowProcesses/WorkflowProcess/Transitions/Transition","Name","name");
			digester.addSetProperties("Package/WorkflowProcesses/WorkflowProcess/Transitions/Transition","From","from");
			digester.addSetProperties("Package/WorkflowProcesses/WorkflowProcess/Transitions/Transition","To","to");
			digester.addBeanPropertySetter("Package/WorkflowProcesses/WorkflowProcess/Transitions/Transition/Condition","condition");
			digester.addSetNext("Package/WorkflowProcesses/WorkflowProcess/Transitions/Transition","addTransition");			

			digester.addObjectCreate("Package/WorkflowProcesses/WorkflowProcess/ExtendedAttributes/ExtendedAttribute",ExtendedAttribute.class);			
			digester.addSetProperties("Package/WorkflowProcesses/WorkflowProcess/ExtendedAttributes/ExtendedAttribute","Name","name");
			digester.addSetProperties("Package/WorkflowProcesses/WorkflowProcess/ExtendedAttributes/ExtendedAttribute","Value","value");
			digester.addSetNext("Package/WorkflowProcesses/WorkflowProcess/ExtendedAttributes/ExtendedAttribute","addExtendedAttribute");			

			digester.addSetNext("Package/WorkflowProcesses/WorkflowProcess","addWorkflowProcess");			

			
			Pack pp = (Pack)digester.parse(FileName);
			return pp;
			
		}catch(Exception exc){
			exc.printStackTrace();
			return null;
		}
		
	}

        public boolean verLicense(String CompCode,String LicenseKey) {
            byte[] lKeys = hexStringToByteArray(LicenseKey);
            byte[] dKeys = cryptDecrypt(lKeys,"VAC");
            String dKey = new String(dKeys);
            int cLen = CompCode.length();
//            System.out.println("[vacwe Info]:" + CompCode);
//            System.out.println("[vacwe Info]:" + LicenseKey);
//            System.out.println("[vacwe Info]:" + dKey);
            if (dKey.length()<cLen+10
                    || !CompCode.equals(dKey.substring(0, cLen))) {
                    System.out.println("[vacwe Info]: * Invalid License *");
                    return false;
            }
            String expiredDate = dKey.substring(cLen, dKey.length());
            if (expiredDate.compareTo(TodayDate("yyyy-MM-dd"))<0) {
               System.out.println("[vacwe Info]: * License Expired *" + expiredDate);
               return false;
            }

            return true;
        }

        public String gen(String CompCode, String ExpDate) {
// -- Test Gen License ---
// byte[] tKeys = Util.toByteArray("KI2099-12-31".toCharArray());
            byte[] tKeys = toByteArray((CompCode+ExpDate).toCharArray());
            byte[] oKeys = cryptEncrypt(tKeys,"VAC");
            String oKey = byteArrayToHexString(oKeys);
            return oKey;
        }

    public static Locale fromString(String locale) {
        String parts[] = locale.split("_", -1);
        if (parts.length == 1) return new Locale(parts[0]);
            else if (parts.length == 2
                || (parts.length == 3 && parts[2].startsWith("#")))
            return new Locale(parts[0], parts[1]);
        else return new Locale(parts[0], parts[1], parts[2]);
    }

    public static String TodayDate(String dateFormat) {
        java.util.Calendar cal = java.util.Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat,fromString("en_US"));
        return sdf.format(cal.getTime());
    }


  public static String byteArrayToHexString(byte[] b){
     StringBuffer sb = new StringBuffer(b.length * 2);
     for (int i = 0; i < b.length; i++){
       int v = b[i] & 0xff;
       if (v < 16) {
         sb.append('0');
       }
       sb.append(Integer.toHexString(v));
     }
     return sb.toString().toUpperCase();
  }

  public static byte[] hexStringToByteArray(String s) {
    int len = s.length();
    byte[] data = new byte[len / 2];
    for (int i = 0; i < len; i += 2) {
        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                             + Character.digit(s.charAt(i+1), 16));
    }
    return data;
}

/**
 * Encrypt the bytes using given password.
 * @param input the bytes that are intended to be encrypted
 * @param pwd the password
 * @return
 */
public static byte[] cryptEncrypt(byte[] input, String pwd) {
	javax.crypto.Cipher cipher = cryptGetCipher(pwd, false);
	byte[] result = null;
	try {
		result = cipher.doFinal(input);
	} catch (javax.crypto.IllegalBlockSizeException ex) {
//		Ts.printErr(ex);
	} catch (javax.crypto.BadPaddingException ex) {
//		Ts.printErr(ex);
	}
	return result;
}

/**
 * Decrypt the bytes using given password.
 * @param cipherText bytes that are intended to be decrypted
 * @param pwd the password
 * @return
 */
public static byte[] cryptDecrypt(byte[] cipherText, String pwd) {
	javax.crypto.Cipher cipher = cryptGetCipher(pwd, true);
	byte[] result = null;
	try {
		result = cipher.doFinal(cipherText);
	} catch (javax.crypto.IllegalBlockSizeException ex) {
//		Ts.printErr(ex);
	} catch (javax.crypto.BadPaddingException ex) {
//		Ts.printErr(ex);
	}
	return result;
}
/**
 * Common part for encryption and decryption.
 * @param pwd the password
 * @param isDecryption <tt>true</tt> if this method is for decryption and <tt>false</tt> if this is for encryption
 * @return
 */
private static javax.crypto.Cipher cryptGetCipher(String pwd, boolean isDecryption) {
	//--- Get the hash algorithm
	java.security.MessageDigest md5 = null;
	try {
		md5 = java.security.MessageDigest.getInstance("MD5");
	} catch (java.security.NoSuchAlgorithmException ex) {
//		Ts.printErr(ex);
	}
	//--- Hash the pwd to make a 128bit key
	byte[] key = md5.digest(pwd.getBytes());
	//--- Create a key suitable for AES
	javax.crypto.spec.SecretKeySpec skey = new javax.crypto.spec.SecretKeySpec(key, "AES");
	javax.crypto.spec.IvParameterSpec ivSpec = new javax.crypto.spec.IvParameterSpec(md5.digest(key));
	javax.crypto.Cipher cipher = null;
	try {
		cipher = javax.crypto.Cipher.getInstance("AES/CTR/NoPadding", "SunJCE");
	} catch (java.security.NoSuchAlgorithmException ex) {
//		Ts.printErr(ex);
	} catch (java.security.NoSuchProviderException ex) {
//		Ts.printErr(ex);
	} catch (javax.crypto.NoSuchPaddingException ex) {
//		Ts.printErr(ex);
	}
	try {
		if (isDecryption) {
			cipher.init(javax.crypto.Cipher.ENCRYPT_MODE, skey, ivSpec);
		} else {
			cipher.init(javax.crypto.Cipher.DECRYPT_MODE, skey, ivSpec);
		}
	} catch (java.security.InvalidKeyException ex) {
//		Ts.printErr(ex);
	} catch (java.security.InvalidAlgorithmParameterException ex) {
//		Ts.printErr(ex);
	}
	return cipher;
}
public static byte[] toByteArray(char[] array) {
    return toByteArray(array, Charset.defaultCharset());
}
public static byte[] toByteArray(char[] array, Charset charset) {
    CharBuffer cbuf = CharBuffer.wrap(array);
    ByteBuffer bbuf = charset.encode(cbuf);
    return bbuf.array();
}

    public String GetData(String aData,int index) {
        // index start with 0 , Delimeter = ';'
        String ss = "";
        int ii = 0;
        int sindex = 0;
        int eindex = 0;
        while (ii<index) {
            // ,
            eindex = aData.indexOf(';', sindex);
            if (eindex<0) {
                return "";
            }
            // Tab
            //eindex = aData.indexOf('\u0009', sindex);
            sindex = eindex + 1;
            ii++;
        }
        eindex = aData.indexOf(';', sindex);
            if (eindex<0) {
                return "";
            }
        ss = aData.substring(sindex, eindex);
        return ss;
    }
    
}
