/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package xpdlparser;

import java.io.Serializable;

/**
 *
 * @author teeramanop
 */
public class Subflow implements Serializable {
	private java.lang.String Id = new String();
	private java.lang.String Execution = new String();

	/**
	 * @return
	 */
	public java.lang.String getExecution() {
		return Execution;
	}

	/**
	 * @return
	 */
	public java.lang.String getId() {
		return Id;
	}

	/**
	 * @param string
	 */
	public void setExecution(java.lang.String string) {
		Execution = string;
	}

	/**
	 * @param string
	 */
	public void setId(java.lang.String string) {
		Id = string;
	}
    
}
