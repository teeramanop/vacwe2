/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package xpdlparser;

import java.io.Serializable;

/**
 *
 * @author teeramanop
 */
public class Transition implements Serializable {
	private java.lang.String Id = new String();
	private java.lang.String Name = new String();
	private java.lang.String From = new String();
	private java.lang.String To = new String();
	private java.lang.String Condition = new String();

	/**
	 * @return
	 */
	public java.lang.String getCondition() {
		return Condition;
	}
	public java.lang.String getFrom() {
		return From;
	}
	public java.lang.String getId() {
		return Id;
	}
	public java.lang.String getName() {
		return Name;
	}
	public java.lang.String getTo() {
		return To;
	}

	/**
	 * @param string
	 */
	public void setCondition(java.lang.String string) {
		Condition = string;
	}
	public void setFrom(java.lang.String string) {
		From = string;
	}
	public void setId(java.lang.String string) {
		Id = string;
	}
	public void setName(java.lang.String string) {
		Name = string;
	}
	public void setTo(java.lang.String string) {
		To = string;
	}
    
}
