/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package xpdlparser;

import java.io.Serializable;

/**
 *
 * @author teeramanop
 */
public class ExtendedAttribute implements Serializable {
	private java.lang.String Name = new String();
	private java.lang.String Value = new String();

	/**
	 * @return
	 */
	public java.lang.String getName() {
		return Name;
	}

	/**
	 * @return
	 */
	public java.lang.String getValue() {
		return Value;
	}

	/**
	 * @param string
	 */
	public void setName(java.lang.String string) {
		Name = string;
	}

	/**
	 * @param string
	 */
	public void setValue(java.lang.String string) {
		Value = string;
	}

}
