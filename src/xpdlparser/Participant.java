/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package xpdlparser;

import java.io.Serializable;

/**
 *
 * @author teeramanop
 */
public class Participant implements Serializable {
	private java.lang.String Id = new String();
	private java.lang.String Name = new String();
	private java.lang.String Type = new String();

	public java.lang.String getId() {
		return Id;
	}
	public java.lang.String getName() {
		return Name;
	}
	public java.lang.String getType() {
		return Type;
	}
	public void setId(java.lang.String string) {
		Id = string;
	}

	public void setName(java.lang.String string) {
		Name = string;
	}

	public void setType(java.lang.String string) {
		Type = string;
	}
	
	public void addParticipantType(ParticipantType participanttype){
		Type = participanttype.getType();
	}
    
}
