/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package xpdlparser;

import java.io.Serializable;
import java.util.Vector;

/**
 *
 * @author teeramanop
 */
public class Activity implements Serializable {
	private java.lang.String Id = new String();
	private java.lang.String Name = new String();
	private java.lang.String Description = new String();
	private java.lang.String Performer = new String();
	private java.lang.String Priority = new String();
	private java.lang.String Icon = new String();
	private Vector Tools;
	private Vector Subflows;

	public Activity() {
		Tools = new Vector();
		Subflows = new Vector();
	}

	public java.lang.String getDescription() {
		return Description;
	}
	public java.lang.String getId() {
		return Id;
	}
	public java.lang.String getName() {
		return Name;
	}
	public java.lang.String getPerformer() {
		return Performer;
	}
	public void setDescription(java.lang.String string) {
		Description = string;
	}

	/**
	 * @param string
	 */
	public void setId(java.lang.String string) {
		Id = string;
	}
	public void setName(java.lang.String string) {
		Name = string;
	}
	public void setPerformer(java.lang.String string) {
		Performer = string;
	}

        public String getPriority() {
            return Priority;
        }

        public void setPriority(String Priority) {
            this.Priority = Priority;
        }

        public String getIcon() {
            return Icon;
        }

        public void setIcon(String Icon) {
            this.Icon = Icon;
        }

	public void addTool(Tool t) {
		Tools.addElement(t);
	}
	public void addSubflow(Subflow s) {
		Subflows.addElement(s);
	}

	/**
	 * @return
	 */
	public Vector getTools() {
		return Tools;
	}
	public Vector getSubflows() {
		return Subflows;
	}
    
}
