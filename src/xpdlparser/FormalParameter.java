/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package xpdlparser;

import java.io.Serializable;

/**
 *
 * @author teeramanop
 */
public class FormalParameter implements Serializable {
	private java.lang.String Id = new String();
	private java.lang.String Index = new String();
	private java.lang.String Mode = new String();
	private java.lang.String DataType = new String();
	private java.lang.String Description = new String();

	public java.lang.String getDataType() {
		return DataType;
	}
	public java.lang.String getDescription() {
		return Description;
	}
	public java.lang.String getId() {
		return Id;
	}
	public java.lang.String getIndex() {
		return Index;
	}
	public java.lang.String getMode() {
		return Mode;
	}

	public void setDataType(java.lang.String string) {
		DataType = string;
	}
	public void setDescription(java.lang.String string) {
		Description = string;
	}
	public void setId(java.lang.String string) {
		Id = string;
	}
	public void setIndex(java.lang.String string) {
		Index = string;
	}
	public void setMode(java.lang.String string) {
		Mode = string;
	}

	public void addDataType(DataType dt){
		DataType = dt.getType();
	}
    
}
