/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package xpdlparser;

import java.io.Serializable;
import java.util.Vector;

/**
 *
 * @author teeramanop
 */
public class Application implements Serializable {
	private java.lang.String Id = new String();
	private java.lang.String Name = new String();
	private java.lang.String Description = new String();
	private Vector FormalParameters;

	public Application() {
		FormalParameters = new Vector();
	}

	public java.lang.String getDescription() {
		return Description;
	}

	public java.lang.String getId() {
		return Id;
	}

	public java.lang.String getName() {
		return Name;
	}

	public void setDescription(java.lang.String string) {
		Description = string;
	}

	public void setId(java.lang.String string) {
		Id = string;
	}

	public void setName(java.lang.String string) {
		Name = string;
	}

	public void addFormalParameter(FormalParameter fp) {
		FormalParameters.addElement(fp);
	}
    
}
