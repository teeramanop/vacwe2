/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package xpdlparser;

import java.io.Serializable;
import java.util.Vector;

/**
 *
 * @author teeramanop
 */
public class Tool implements Serializable {
	private java.lang.String Id = new String();
	private java.lang.String Type = new String();
	private Vector ActualParameters;

	public Tool() {
		ActualParameters = new Vector();
	}

	public java.lang.String getId() {
		return Id;
	}
	public java.lang.String getType() {
		return Type;
	}
	public void setId(java.lang.String string) {
		Id = string;
	}
	public void setType(java.lang.String string) {
		Type = string;
	}

	public void addActualParameter(ActualParameter ap) {
		ActualParameters.addElement(ap);
	}
    
}
