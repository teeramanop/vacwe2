/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package xpdlparser;

import java.io.Serializable;
import java.util.Vector;

/**
 *
 * @author teeramanop
 */
public class Pack implements Serializable {
	private String Id = new String();
	private String Name = new String();
	private String Created = new String();
	private Vector Participants;
	private Vector Applications;
	private Vector WorkflowProcesses;

	public Pack() {
		Participants = new Vector();
		Applications = new Vector();
		WorkflowProcesses = new Vector();
	}

	public java.lang.String getId() {
		return Id;
	}
	public java.lang.String getName() {
		return Name;
	}
	public java.lang.String getCreated() {
		return Created;
	}
	public void setId(java.lang.String string) {
		Id = string;
	}
	public void setName(java.lang.String string) {
		Name = string;
	}
	public void setCreated(java.lang.String string) {
		Created = string;
	}

	public void addParticipant(Participant p) {
		Participants.addElement(p);
	}
	public void addApplication(Application appl) {
		Applications.addElement(appl);
	}
	public void addWorkflowProcess(WorkflowProcess wfp) {
		WorkflowProcesses.addElement(wfp);
	}
	public void addPackHeader(PackHeader packheader){
		Created = packheader.getCreated();
	}

	/**
	 * @return
	 */
	public Vector getWorkflowProcesses() {
		return WorkflowProcesses;
	}

	/**
	 * @return
	 */
	public Vector getApplications() {
		return Applications;
	}

	/**
	 * @return
	 */
	public Vector getParticipants() {
		return Participants;
	}
    
}
