/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package xpdlparser;

import java.io.Serializable;
import java.util.Vector;

/**
 *
 * @author teeramanop
 */
public class WorkflowProcess implements Serializable {
	private java.lang.String Id = new String();
	private java.lang.String Name = new String();
	private Vector DataFields;
	private Vector Activities;
	private Vector Transitions;
	private Vector ExtendedAttributes;

	public WorkflowProcess() {
		DataFields = new Vector();
		Activities = new Vector();
		Transitions = new Vector();
		ExtendedAttributes = new Vector();
	}

	public java.lang.String getId() {
		return Id;
	}
	public java.lang.String getName() {
		return Name;
	}

	/**
	 * @param string
	 */
	public void setId(java.lang.String string) {
		Id = string;
	}
	public void setName(java.lang.String string) {
		Name = string;
	}

	public void addDataField(DataField df) {
		DataFields.addElement(df);
	}
	public void addActivity(Activity at) {
		Activities.addElement(at);
	}
	public void addTransition(Transition ts) {
		Transitions.addElement(ts);
	}
	public void addExtendedAttribute(ExtendedAttribute ea) {
		ExtendedAttributes.addElement(ea);
	}

	/**
	 * @return
	 */
	public Vector getActivities() {
		return Activities;
	}

	/**
	 * @return
	 */
	public Vector getDataFields() {
		return DataFields;
	}

	/**
	 * @return
	 */
	public Vector getExtendedAttributes() {
		return ExtendedAttributes;
	}

	/**
	 * @return
	 */
	public Vector getTransitions() {
		return Transitions;
	}
    
}
