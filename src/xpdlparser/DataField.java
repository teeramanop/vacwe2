/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package xpdlparser;

/**
 *
 * @author teeramanop
 */
public class DataField {
	private java.lang.String Id = new String();
	private java.lang.String IsArray = new String();
	private java.lang.String Name = new String();
	private java.lang.String DataType = new String();
	private java.lang.String Description = new String();

	public java.lang.String getDataType() {
		return DataType;
	}
	public java.lang.String getDescription() {
		return Description;
	}
	public java.lang.String getId() {
		return Id;
	}
	public java.lang.String getIsArray() {
		return IsArray;
	}
	public java.lang.String getName() {
		return Name;
	}

	/**
	 * @param string
	 */
	public void setDataType(java.lang.String string) {
		DataType = string;
	}
	public void setDescription(java.lang.String string) {
		Description = string;
	}
	public void setId(java.lang.String string) {
		Id = string;
	}
	public void setIsArray(java.lang.String string) {
		IsArray = string;
	}
	public void setName(java.lang.String string) {
		Name = string;
	}

	public void addDataType(DataType dt){
		DataType = dt.getType();
	}
    
}
