/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import xpdlparser.Activity;
import xpdlparser.Pack;
import xpdlparser.Tool;
import xpdlparser.Transition;
import xpdlparser.WorkflowProcess;

public class Activities implements Serializable {

    private int Reccount = 0;

	private String[] WorkflowProcessId = null;
	private String[] WorkflowProcessName = null;
	private String[] Id = null;
	private String[] Name = null;
	private String[] Desc = null;
	private String[] Performer = null;
	private String[] Priority = null;
	private String[] Icon = null;
	private String[] ToolId = null;

    public int getReccount() {
        return Reccount;
    }

    public void setReccount(int Reccount) {
        this.Reccount = Reccount;
    }

    public String[] getWorkflowProcessId() {
        return WorkflowProcessId;
    }

    public void setWorkflowProcessId(String[] WorkflowProcessId) {
        this.WorkflowProcessId = WorkflowProcessId;
    }

    public String[] getWorkflowProcessName() {
        return WorkflowProcessName;
    }

    public void setWorkflowProcessName(String[] WorkflowProcessName) {
        this.WorkflowProcessName = WorkflowProcessName;
    }

    public String[] getId() {
        return Id;
    }

    public void setId(String[] Id) {
        this.Id = Id;
    }

    public String[] getName() {
        return Name;
    }

    public void setName(String[] Name) {
        this.Name = Name;
    }

    public String[] getDesc() {
        return Desc;
    }

    public void setDesc(String[] Desc) {
        this.Desc = Desc;
    }

    public String[] getPerformer() {
        return Performer;
    }

    public void setPerformer(String[] Performer) {
        this.Performer = Performer;
    }

    public void setPriority(String[] Priority) {
        this.Priority = Priority;
    }

    public void setIcon(String[] Icon) {
        this.Icon = Icon;
    }

    public String[] getToolId() {
        return ToolId;
    }

    public String[] getIcon() {
        return Icon;
    }

    public void setToolId(String[] ToolId) {
        this.ToolId = ToolId;
    }
        
    public void SelectActByRoles(Pack pkg,ArrayList aRole,int aRoleCount) {
        int ii,jj,kk,count;
	int wfpSize,actSize;
	ArrayList t_WorkflowProcessId = new ArrayList();
	ArrayList t_WorkflowProcessName = new ArrayList();
	ArrayList t_Id = new ArrayList();
	ArrayList t_Name = new ArrayList();
	ArrayList t_Desc = new ArrayList();
	ArrayList t_Performer = new ArrayList();
	ArrayList t_Priority = new ArrayList();
	ArrayList t_Icon = new ArrayList();
	ArrayList t_ToolId = new ArrayList();
	try {
            count = 0;
	    wfpSize = pkg.getWorkflowProcesses().size();
            ii = 0;
            while(ii < wfpSize){
                WorkflowProcess wfp = new WorkflowProcess();
		wfp =(WorkflowProcess)pkg.getWorkflowProcesses().get(ii);
                actSize = wfp.getActivities().size();
                //
                jj = 0;
                
		while(jj < actSize){
                    Activity act = new Activity();
		    act = (Activity)wfp.getActivities().get(jj);
		    kk = 0;
                    while(kk < aRoleCount){
                        if (act.getPerformer().equals(aRole.get(kk))) {
                            t_WorkflowProcessId.add(wfp.getId());			
                            t_WorkflowProcessName.add(wfp.getName());			
                            t_Id.add(act.getId());			
                            t_Name.add(act.getName());			
                            t_Desc.add(act.getDescription());			
                            t_Performer.add(act.getPerformer());
                            t_Priority.add(act.getPriority());
                            t_Icon.add(act.getIcon());
                            if (act.getTools().size()>0) {
				Tool tool = new Tool();
				tool = (Tool)act.getTools().get(0);
				t_ToolId.add(tool.getId());	    	
                            }
                            else {
				t_ToolId.add("");			
                            }
                            count++;
			}
			kk++;
                    }
				
                    jj++;
		}
		ii++;
            }
//            aActivities.setReccount(count);
//            Object[] aObjs = t_WorkflowProcessId.toArray();
//            aActivities.setWorkflowProcessId(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_WorkflowProcessName.toArray();
//            aActivities.setWorkflowProcessName(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_Id.toArray();
//            aActivities.setId(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_Name.toArray();
//            aActivities.setName(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_Desc.toArray();
//            aActivities.setDesc(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_Performer.toArray();
//            aActivities.setPerformer(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_Priority.toArray();
//            aActivities.setPriority(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_Icon.toArray();
//            aActivities.setIcon(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_ToolId.toArray();
//            aActivities.setToolId(Arrays.copyOf(aObjs, aObjs.length, String[].class));
            Reccount = count;
            Object[] aObjs = t_WorkflowProcessId.toArray();
            WorkflowProcessId = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_WorkflowProcessName.toArray();
            WorkflowProcessName = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_Id.toArray();
            Id = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_Name.toArray();
            Name = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_Desc.toArray();
            Desc = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_Performer.toArray();
            Performer = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_Priority.toArray();
            Priority = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_Icon.toArray();
            Icon = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_ToolId.toArray();
            ToolId = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
        }
	catch(Exception exc){
            exc.printStackTrace();
	}

    }
    public void SelectActByRole(Pack pkg,String aRole) {
        int ii,jj,count;
	int wfpSize,actSize;
	ArrayList t_WorkflowProcessId = new ArrayList();
	ArrayList t_WorkflowProcessName = new ArrayList();
	ArrayList t_Id = new ArrayList();
	ArrayList t_Name = new ArrayList();
	ArrayList t_Desc = new ArrayList();
	ArrayList t_Performer = new ArrayList();
	ArrayList t_Priority = new ArrayList();
	ArrayList t_Icon = new ArrayList();
	ArrayList t_ToolId = new ArrayList();
        try {
            count = 0;
            wfpSize = pkg.getWorkflowProcesses().size();
            ii = 0;
            while(ii < wfpSize){
                WorkflowProcess wfp = new WorkflowProcess();
		wfp =(WorkflowProcess)pkg.getWorkflowProcesses().get(ii);
		actSize = wfp.getActivities().size();
		//
		jj = 0;
		while(jj < actSize){
                    Activity act = new Activity();
                    act = (Activity)wfp.getActivities().get(jj);
                    if (act.getPerformer().equals(aRole)) {
                    	t_WorkflowProcessId.add(wfp.getId());			
			t_WorkflowProcessName.add(wfp.getName());			
			t_Id.add(act.getId());			
			t_Name.add(act.getName());			
			t_Desc.add(act.getDescription());			
			t_Performer.add(act.getPerformer());
			t_Priority.add(act.getPriority());
			t_Icon.add(act.getIcon());
			if (act.getTools().size()>0) {
                            Tool tool = new Tool();
                            tool = (Tool)act.getTools().get(0);
                            t_ToolId.add(tool.getId());	    	
                        }
			else {
                            t_ToolId.add("");			
			}
			count++;
                    }
                    jj++;
		}
		ii++;
            }
//            aActivities.setReccount(count);
//            Object[] aObjs = t_WorkflowProcessId.toArray();
//            aActivities.setWorkflowProcessId(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_WorkflowProcessName.toArray();
//            aActivities.setWorkflowProcessName(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_Id.toArray();
//            aActivities.setId(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_Name.toArray();
//            aActivities.setName(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_Desc.toArray();
//            aActivities.setDesc(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_Performer.toArray();
//            aActivities.setPerformer(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_Priority.toArray();
//            aActivities.setPriority(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_Icon.toArray();
//            aActivities.setIcon(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_ToolId.toArray();
//            aActivities.setToolId(Arrays.copyOf(aObjs, aObjs.length, String[].class));
            Reccount = count;
            Object[] aObjs = t_WorkflowProcessId.toArray();
            WorkflowProcessId = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_WorkflowProcessName.toArray();
            WorkflowProcessName = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_Id.toArray();
            Id = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_Name.toArray();
            Name = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_Desc.toArray();
            Desc = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_Performer.toArray();
            Performer = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_Priority.toArray();
            Priority = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_Icon.toArray();
            Icon = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_ToolId.toArray();
            ToolId = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
	}
	catch(Exception exc){
            exc.printStackTrace();
	}

    }
        
    public void SelectToByCondition(Pack pkg,String f_ProcessId,String f_ActivityId,String condition) {
        int ii,jj,count;
	int wfpSize,trnSize;
	ArrayList t_WorkflowProcessId = new ArrayList();
	ArrayList t_WorkflowProcessName = new ArrayList();
	ArrayList t_Id = new ArrayList();
	ArrayList t_Name = new ArrayList();
	ArrayList t_Desc = new ArrayList();
	ArrayList t_Performer = new ArrayList();
	ArrayList t_Priority = new ArrayList();
	ArrayList t_Icon = new ArrayList();
	ArrayList t_ToolId = new ArrayList();
        try {
            count = 0;
            wfpSize = pkg.getWorkflowProcesses().size();
            ii = 0;
            while(ii < wfpSize){
                WorkflowProcess wfp = new WorkflowProcess();
		wfp =(WorkflowProcess)pkg.getWorkflowProcesses().get(ii);
                if (wfp.getId().equals(f_ProcessId)) {
                    trnSize = wfp.getTransitions().size();
                    jj = 0;
                    while(jj < trnSize){
                        Transition trn = new Transition();
                        trn=(Transition)wfp.getTransitions().get(jj);
                        if (trn.getFrom().equals(f_ActivityId)){
                            if (trn.getCondition().equals(condition)) {
                                t_WorkflowProcessId.add(wfp.getId());			
                                t_WorkflowProcessName.add(wfp.getName());			
                                t_Id.add(trn.getTo());			
                                t_Name.add("");			
                                t_Desc.add("");			
                                t_Performer.add("");
                                t_Priority.add("");
                                t_Icon.add("");
                                t_ToolId.add("");   
                                count++;
                            }
                        }
                        jj++;
                    }
                }
                ii++;
            }
//            aActivities.setReccount(count);
//            Object[] aObjs = t_WorkflowProcessId.toArray();
//            aActivities.setWorkflowProcessId(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_WorkflowProcessName.toArray();
//            aActivities.setWorkflowProcessName(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_Id.toArray();
//            aActivities.setId(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_Name.toArray();
//            aActivities.setName(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_Desc.toArray();
//            aActivities.setDesc(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_Performer.toArray();
//            aActivities.setPerformer(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_Priority.toArray();
//            aActivities.setPriority(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_Icon.toArray();
//            aActivities.setIcon(Arrays.copyOf(aObjs, aObjs.length, String[].class));
//            aObjs = t_ToolId.toArray();
//            aActivities.setToolId(Arrays.copyOf(aObjs, aObjs.length, String[].class));
            
            Reccount = count;
            Object[] aObjs = t_WorkflowProcessId.toArray();
            WorkflowProcessId = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_WorkflowProcessName.toArray();
            WorkflowProcessName = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_Id.toArray();
            Id = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_Name.toArray();
            Name = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_Desc.toArray();
            Desc = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_Performer.toArray();
            Performer = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_Priority.toArray();
            Priority = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_Icon.toArray();
            Icon = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
            aObjs = t_ToolId.toArray();
            ToolId = (Arrays.copyOf(aObjs, aObjs.length, String[].class));
        }
	catch(Exception exc){
            exc.printStackTrace();
	}
    
    }
    
}
