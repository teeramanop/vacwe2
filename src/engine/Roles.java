/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import engine.Participants;
import java.io.Serializable;
import java.util.ArrayList;
import engine.datactrl.Userrole;
import engine.datactrl.UserroleCtrl;
import xpdlparser.Pack;

/**
 *
 * @author Teeramanop
 */
public class Roles implements Serializable {
    public Roles() {
    }
   	private String[] Role = null;
	private String[] RoleDesc = null;
	private String RetCode = new String();
	private String RetDesc = new String();
	private int Reccount;
	public int getReccount() {
		return Reccount;
	}
	public String getRetCode() {
		return RetCode;
	}
	public String getRetDesc() {
		return RetDesc;
	}
	public String[] getRole() {
		return Role;
	}
	public String getRole(int index) {
		return getRole()[index];
	}
	public String[] getRoleDesc() {
		return RoleDesc;
	}
	public String getRoleDesc(int index) {
		return getRoleDesc()[index];
	}
	public void setReccount(int i) {
		Reccount = i;
	}

	public void setRetCode(java.lang.String string) {
		RetCode = string;
	}
	public void setRetDesc(java.lang.String string) {
		RetDesc = string;
	}
	public void setRole(java.lang.String[] strings) {
		Role = strings;
	}
	public void setRole(int index, java.lang.String role) {
		Role[index] = role;
	}
	public void setRoleDesc(java.lang.String[] strings) {
		RoleDesc = strings;
	}
	public void setRoleDesc(int index, java.lang.String roledesc) {
		RoleDesc[index] = roledesc;
	}

    public void selectRoles(Roles aRoles,Pack pkg) {

	int ii;
	try {
		Participants aParticipants = new Participants();
		aParticipants.selectParticipants(pkg);
		Reccount = aParticipants.getReccount();
		if (Reccount != 0) {
			aRoles.setRole(new String[Reccount]);
			aRoles.setRoleDesc(new String[Reccount]);
		}
		ii = 0;
		while(ii < Reccount){
                    aRoles.setRole(ii, aParticipants.getId(ii));
         	    aRoles.setRoleDesc(ii, aParticipants.getName(ii));
                    ii++;  
		}
		aRoles.setReccount(ii);
		aRoles.setRetCode("0");
		aRoles.setRetDesc("Completed");

		}
		catch(Exception exc){
			exc.printStackTrace();
		}
        }
        

        public void selectNotinUser_Role(Roles aRoles,Pack pkg,String UserId) {
	int ii,jj,count,roleSize;

        ArrayList t_ParticipantId = new ArrayList();
	ArrayList t_ParticipantName = new ArrayList();

	ArrayList t_Role = new ArrayList();

	try {
            UserroleCtrl aUserRoleCtrl = new UserroleCtrl();
            Userrole[] UserRoles = aUserRoleCtrl.SelectByUserID(UserId);

            ii=0;
            while (ii<UserRoles.length) {
		t_Role.add(UserRoles[ii].getUserrolePK().getRole());
                ii++;
            }
	    roleSize = t_Role.size();

            Participants aParticipants = new Participants();
            aParticipants.selectParticipants(pkg);
	    ii = 0;
	    count = 0;
	    boolean eq = false; 
	    while(ii < aParticipants.getReccount()){
		jj = 0;
		eq = false; 
		while(jj < roleSize){
		   if (t_Role.get(jj).equals(aParticipants.getId(ii))) {
                	  eq = true; 
		   }
		   jj++;
		}
		if (!eq){
			t_ParticipantId.add(aParticipants.getId(ii));			
			t_ParticipantName.add(aParticipants.getName(ii));
			count++;			
		}
		ii++;  
            }
        
    	    aRoles.setReccount(count);
    	    aRoles.setRole(new String[count]);
	    aRoles.setRoleDesc(new String[count]);
            ii = 0;
            while(ii<Reccount){
		aRoles.setRole(ii, (String)t_ParticipantId.get(ii));
		aRoles.setRoleDesc(ii, (String)t_ParticipantName.get(ii));
		ii++;
            }
            aRoles.setRetCode("0");
            aRoles.setRetDesc("Completed");
	}
	catch(Exception exc){
	}
  }       
    
}
