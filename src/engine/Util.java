/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Util {
    public static Locale fromString(String locale) {
        String parts[] = locale.split("_", -1);
        if (parts.length == 1) return new Locale(parts[0]);
            else if (parts.length == 2
                || (parts.length == 3 && parts[2].startsWith("#")))
            return new Locale(parts[0], parts[1]);
        else return new Locale(parts[0], parts[1], parts[2]);
    }
    
    public static String TodayDate(String dateFormat) {
        java.util.Calendar cal = java.util.Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat,fromString("en_US"));
        return sdf.format(cal.getTime());
    }
    public static String DateToString(Date aDate,String dateFormat) {
        if (aDate==null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat,fromString("en_US"));
        return sdf.format(aDate);
    }
    
    public static String DateToString(Date aDate,String dateFormat,String local) {
        if (aDate==null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat,fromString(local));
        return sdf.format(aDate);
    }
    
}
