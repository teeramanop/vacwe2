/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.datactrl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class UseractivityCtrl {
    public UseractivityCtrl() {
    }
 private EntityManagerFactory emf;
private EntityManager getEntityManager() {
if(emf == null) {
    emf = Persistence.createEntityManagerFactory("PU");
}
return emf.createEntityManager();
}

public boolean add(Useractivity aUseractivity) {
EntityManager em = getEntityManager();
try{
    em.getTransaction().begin();
    em.persist(aUseractivity);
    em.getTransaction().commit();
    return true;
    }
catch(Throwable theException) {
    return false;
    }
finally {
    em.close();
    }
}

public boolean add(Useractivity aUseractivity,EntityManager em) {
try{
    em.persist(aUseractivity);
    return true;
    }
catch(Throwable theException) {
    return false;
    }
finally {
    }
}

public boolean remove(Useractivity aUseractivity) {
EntityManager em = getEntityManager();
try{
    em.getTransaction().begin();
    Useractivity aUseractivityx = em.find(Useractivity.class, aUseractivity.getUseractivityPK());
    em.remove(aUseractivityx);
    em.getTransaction().commit();
    return true;
    }
catch(Throwable theException) {
    return false;
    }
finally {
    em.close();
    }
}
    public boolean update(Useractivity aUseractivity) {
        EntityManager em = getEntityManager();
        try{
            em.getTransaction().begin();
            Useractivity aUseractivityx = em.find(Useractivity.class, aUseractivity.getUseractivityPK());
            // ***
            aUseractivityx.setTimeStamp(aUseractivity.getTimeStamp());
            em.getTransaction().commit();
            return true;
        }
        catch(Throwable theException) {
            return false;
        }
        finally {
            em.close();
        }
    }
    
    public boolean update(Useractivity aUseractivity,EntityManager em) {
        try{
            Useractivity aUseractivityx = em.find(Useractivity.class, aUseractivity.getUseractivityPK());
            // ***
            aUseractivityx.setTimeStamp(aUseractivity.getTimeStamp());
            return true;
        }
        catch(Throwable theException) {
            return false;
        }
        finally {
        }
    }

public Useractivity Select(UseractivityPK aPK) {
EntityManager em = getEntityManager();
try{
    Query q = em.createQuery("select c from Useractivity c WHERE " +
            "c.useractivityPK.userId = :UID " +
            "and c.useractivityPK.processId = :PID " +
            "and c.useractivityPK.activityId = :AID");
    q.setParameter("UID",aPK.getUserId());
    q.setParameter("PID",aPK.getProcessId());
    q.setParameter("AID",aPK.getActivityId());

    return (Useractivity) q.getSingleResult();
    } 
    catch(Throwable theException) {
    return null;
    }
    finally {
    em.close();
    }
}
public Useractivity Select(UseractivityPK aPK,EntityManager em) {
try{
    Query q = em.createQuery("select c from Useractivity c WHERE " +
            "c.useractivityPK.userId = :UID " +
            "and c.useractivityPK.processId = :PID " +
            "and c.useractivityPK.activityId = :AID");
    q.setParameter("UID",aPK.getUserId());
    q.setParameter("PID",aPK.getProcessId());
    q.setParameter("AID",aPK.getActivityId());

    return (Useractivity) q.getSingleResult();
    } 
    catch(Throwable theException) {
    return null;
    }
    finally {
    }
}

public Useractivity[] SelectByActivity(String ProcessId,String ActivityId) {
EntityManager em = getEntityManager();
try{
    Query q = em.createQuery("select c from Useractivity c WHERE " + 
            "c.useractivityPK.processId = :PID " +
            "and c.useractivityPK.activityId = :AID " +
            "ORDER BY c.timeStamp");
    
    q.setParameter("PID",ProcessId);
    q.setParameter("AID",ActivityId);
   
    return (Useractivity[]) q.getResultList().toArray(new Useractivity[0]);
    } 
    catch(Throwable theException) {
        return null;
    } finally {
        em.close();
    }
}

    
}
