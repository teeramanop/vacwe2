/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.datactrl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class QueuelogCtrl {
    public QueuelogCtrl() {
    }
    
    private EntityManagerFactory emf;
    private EntityManager getEntityManager() {
        if(emf == null) {
        emf = Persistence.createEntityManagerFactory("PU");
    }
    return emf.createEntityManager();
    }

    public boolean add(Queuelog aQueuelog) {
        EntityManager em = getEntityManager();
        try{
            em.getTransaction().begin();
            em.persist(aQueuelog);
            em.getTransaction().commit();
            return true;
        }
        catch(Throwable theException) {
            return false;
        }
        finally {
            em.close();
        }
    }
    public boolean add(Queuelog aQueuelog,EntityManager em) {
        try{
            em.persist(aQueuelog);
            return true;
        }
        catch(Throwable theException) {
            return false;
        }
        finally {
        }
    }

    public boolean remove(Queuelog aQueuelog) {
    EntityManager em = getEntityManager();
    try{
        em.getTransaction().begin();
        Queuelog aQueuelogx = em.find(Queuelog.class, aQueuelog.getQueuelogPK());
        em.remove(aQueuelogx);
        em.getTransaction().commit();
        return true;
    }
    catch(Throwable theException) {
        return false;
    }
    finally {
        em.close();
    }
    }

    public Queuelog Select(QueuelogPK aPK) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select c from Queuelog c WHERE " +
            "c.queuelogPK.jobId = :JID " +
            "and c.queuelogPK.trnsType = :TTYPE " +
            "and c.queuelogPK.stampTime = :STIME");
            
            q.setParameter("JID",aPK.getJobId());
            q.setParameter("STIME",aPK.getStampTime());
            q.setParameter("TTYPE",aPK.getTrnsType());

            return (Queuelog) q.getSingleResult();
        } 
        catch(Throwable theException) {
            return null;
        }
        finally {
            em.close();
        }
    }

    public Queuelog[] SelectByJob(String JobId) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select c from Queuelog c WHERE " + 
            "c.queuelogPK.jobId = :JID " +
            "ORDER BY c.queuelogPK.stampTime");
    
            q.setParameter("JID",JobId);
   
            return (Queuelog[]) q.getResultList().toArray(new Queuelog[0]);
        } 
        catch(Throwable theException) {
            return null;
        } finally {
            em.close();
        }
    }
    
}
