/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.datactrl;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class MainqueueCtrl {
    public MainqueueCtrl() {
    }
    private EntityManagerFactory emf;
    private EntityManager getEntityManager() {
        if(emf == null) {
            emf = Persistence.createEntityManagerFactory("PU");
        }
        return emf.createEntityManager();
    }
    public boolean add(Mainqueue aMainqueue) {
        EntityManager em = getEntityManager();
        try{
            em.getTransaction().begin();
            // Intial version
            aMainqueue.setVersion(0);
            //
            em.persist(aMainqueue);
            em.getTransaction().commit();
            return true;
        }
    catch(Throwable theException) {
        return false;
        }
    finally {
        em.close();
        }
    }
    public boolean add(Mainqueue aMainqueue,EntityManager em) {
        try{
            if (Select(aMainqueue.getJobId(), em)!=null) {
                return false;
            }
            // Intial version
            aMainqueue.setVersion(0);
            //
            em.persist(aMainqueue);

            return true;
        }
    catch(Throwable theException) {
        return false;
        }
    finally {
        }
    }
    
    public boolean remove(Mainqueue aMainqueue) {
        EntityManager em = getEntityManager();
        try{
            em.getTransaction().begin();
            Mainqueue aMainqueuex = em.find(Mainqueue.class, aMainqueue.getJobId());
            em.remove(aMainqueuex);
            em.getTransaction().commit();
            return true;
            }
        catch(Throwable theException) {
            return false;
        }
        finally {
            em.close();
        }
    }
    public boolean remove(Mainqueue aMainqueue,EntityManager em) {
        try{
            Mainqueue aMainqueuex = em.find(Mainqueue.class, aMainqueue.getJobId());
            em.remove(aMainqueuex);
            return true;
            }
        catch(Throwable theException) {
            return false;
        }
        finally {
        }
    }

    public boolean update(Mainqueue aMainqueue) {
        EntityManager em = getEntityManager();
        try{
            em.getTransaction().begin();
            Mainqueue aMainqueuex = em.find(Mainqueue.class, aMainqueue.getJobId());
            // *** Check version
            if (!aMainqueue.getVersion().equals(aMainqueuex.getVersion())) {
                // Different verion, cannot update. (Someone already updated this reccord)
                return false;
            }
            // Increase version
            int version = aMainqueuex.getVersion();
            version++;
            if (version>100) {
                version = 0;
            }
            aMainqueuex.setVersion(version);
            // ***
            aMainqueuex.setProcessId(aMainqueue.getProcessId());
            aMainqueuex.setActivityId(aMainqueue.getActivityId());
            aMainqueuex.setSubflow(aMainqueue.getSubflow());
            aMainqueuex.setParentProcessId(aMainqueue.getParentProcessId());
            aMainqueuex.setParentActivityId(aMainqueue.getParentActivityId());
            aMainqueuex.setHoldBy(aMainqueue.getHoldBy());
            aMainqueuex.setHoldTime(aMainqueue.getHoldTime());
            aMainqueuex.setUserId(aMainqueue.getUserId());
            em.getTransaction().commit();
            return true;
        }
        catch(Throwable theException) {
            return false;
        }
        finally {
            em.close();
        }
    }
    public boolean update(Mainqueue aMainqueue,EntityManager em) {
        try{
            Mainqueue aMainqueuex = em.find(Mainqueue.class, aMainqueue.getJobId());
            // *** Check version
            if (!aMainqueue.getVersion().equals(aMainqueuex.getVersion())) {
                // Different verion, cannot update. (Someone already updated this reccord)
                return false;
            }
            // Increase version
            int version = aMainqueuex.getVersion();
            version++;
            if (version>100) {
                version = 0;
            }
            aMainqueuex.setVersion(version);
            // ***
            aMainqueuex.setProcessId(aMainqueue.getProcessId());
            aMainqueuex.setActivityId(aMainqueue.getActivityId());
            aMainqueuex.setSubflow(aMainqueue.getSubflow());
            aMainqueuex.setParentProcessId(aMainqueue.getParentProcessId());
            aMainqueuex.setParentActivityId(aMainqueue.getParentActivityId());
            aMainqueuex.setHoldBy(aMainqueue.getHoldBy());
            aMainqueuex.setHoldTime(aMainqueue.getHoldTime());
            aMainqueuex.setUserId(aMainqueue.getUserId());

            return true;
        }
        catch(Throwable theException) {
            return false;
        }
        finally {
        }
    }

    public Mainqueue Select(String JobId) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select c from Mainqueue c WHERE c.jobId = :JID");
            q.setParameter("JID",JobId);

            return (Mainqueue) q.getSingleResult();
        } 
        catch(Throwable theException) {
            return null;
        }
        finally {
            em.close();
        }
    }
    public Mainqueue Select(String JobId,EntityManager em) {
        try{
            Query q = em.createQuery("select c from Mainqueue c WHERE c.jobId = :JID");
            q.setParameter("JID",JobId);

            return (Mainqueue) q.getSingleResult();
        } 
        catch(Throwable theException) {
            return null;
        }
        finally {
        }
    }

    public Mainqueue[] SelectAll() {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select c from Mainqueue c "+
                    "ORDER BY c.jobId");
    
            return (Mainqueue[]) q.getResultList().toArray(new Mainqueue[0]);
        }
        finally {
            em.close();
        }
    }   

    public Mainqueue[] SelectByOrgCodeProcessIdActivityId(String OrgCode,String ProcessId,String ActivityId) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select c from Mainqueue c WHERE "+
                    "c.processId = :PID "+
                    "and c.activityId = :AID "+
                    "and c.jobId LIKE :OCODE "+
                    "ORDER BY c.jobId");
    
            q.setParameter("PID",ProcessId);
            q.setParameter("AID",ActivityId);
            q.setParameter("OCODE",OrgCode+"%");
    
            return (Mainqueue[]) q.getResultList().toArray(new Mainqueue[0]);
        }
        finally {
            em.close();
        }
    }   
    public Mainqueue[] SelectByOrgCodeProcessIdActivityId(String OrgCode,String ProcessId,String ActivityId,EntityManager em) {
        try{
            Query q = em.createQuery("select c from Mainqueue c WHERE "+
                    "c.processId = :PID "+
                    "and c.activityId = :AID "+
                    "and c.jobId LIKE :OCODE "+
                    "ORDER BY c.jobId");
    
            q.setParameter("PID",ProcessId);
            q.setParameter("AID",ActivityId);
            q.setParameter("OCODE",OrgCode+"%");
    
            return (Mainqueue[]) q.getResultList().toArray(new Mainqueue[0]);
        }
        finally {
        }
    }   
    
    public Mainqueue[] SelectByProcessIdActivityId(String ProcessId,String ActivityId) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select c from Mainqueue c WHERE c.processId = :PID and c.activityId = :AID ORDER BY c.jobId");
    
            q.setParameter("PID",ProcessId);
            q.setParameter("AID",ActivityId);
    
            return (Mainqueue[]) q.getResultList().toArray(new Mainqueue[0]);
        }
        finally {
            em.close();
        }
    }   
    public Mainqueue[] SelectByProcessIdActivityId(String ProcessId,String ActivityId,EntityManager em) {
        try{
            Query q = em.createQuery("select c from Mainqueue c WHERE c.processId = :PID and c.activityId = :AID ORDER BY c.jobId");
    
            q.setParameter("PID",ProcessId);
            q.setParameter("AID",ActivityId);
    
            return (Mainqueue[]) q.getResultList().toArray(new Mainqueue[0]);
        }
        finally {
        }
    }
    
public boolean clearHoldByUser(String HoldBy) {
EntityManager em = getEntityManager();
try{
    em.getTransaction().begin();
    Query q = em.createQuery("update Mainqueue c set c.holdBy = '',c.userId = '' WHERE " +
            "c.holdBy = :HBY ");

    q.setParameter("HBY",HoldBy);
    q.executeUpdate();

    em.getTransaction().commit();
    
    return true;
    }
catch(Throwable theException) {
    return false;
    }
finally {
    em.close();
    }
}
public boolean clearHoldByUser(String HoldBy,EntityManager em) {
try{
    Query q = em.createQuery("update Mainqueue c set c.holdBy = '',c.userId = '' WHERE " +
            "c.holdBy = :HBY ");

    q.setParameter("HBY",HoldBy);
    q.executeUpdate();

    return true;
    }
catch(Throwable theException) {
    return false;
    }
finally {
    }
}

//public boolean clearAllHoldBy() {
//EntityManager em = getEntityManager();
//try{
//    em.getTransaction().begin();    
//    Query q = em.createQuery("update Mainqueue c set c.holdBy = '' ");
//
//    q.executeUpdate();
//
//    em.getTransaction().commit();
//    
//    return true;
//    }
//catch(Throwable theException) {
//    return false;
//    }
//finally {
//    em.close();
//    }
//}
   
    public Mainqueue[] SelectByUser(String ProcessId,String ActivityId,String UserId) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select c from Mainqueue c WHERE " +
                    "c.processId = :PID " +
                    "and c.activityId = :AID " +
                    "and c.userId = :UID " +
                    "ORDER BY c.jobId");
    
            q.setParameter("PID",ProcessId);
            q.setParameter("AID",ActivityId);
            q.setParameter("UID",UserId);
    
            return (Mainqueue[]) q.getResultList().toArray(new Mainqueue[0]);
        }
        finally {
            em.close();
        }
    }   
    
    public Mainqueue[] SelectByUser(String OrgCode,String ProcessId,String ActivityId,String UserId) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select c from Mainqueue c WHERE " +
                    "c.processId = :PID " +
                    "and c.activityId = :AID " +
                    "and c.userId = :UID " +
                    "and c.jobId LIKE :OCODE "+
                    "ORDER BY c.jobId");
    
            q.setParameter("PID",ProcessId);
            q.setParameter("AID",ActivityId);
            q.setParameter("UID",UserId);
            q.setParameter("OCODE",OrgCode+"%");
    
            return (Mainqueue[]) q.getResultList().toArray(new Mainqueue[0]);
        }
        finally {
            em.close();
        }
    }   

    public Mainqueue[] SelectFromPool(String OrgCode,String ProcessId,String ActivityId) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select c from Mainqueue c WHERE " +
                    "c.processId = :PID " +
                    "and c.activityId = :AID " +
                    "and c.userId = '' " +
                    "and c.jobId LIKE :OCODE "+
                    "ORDER BY c.jobId");
    
            q.setParameter("PID",ProcessId);
            q.setParameter("AID",ActivityId);
            q.setParameter("OCODE",OrgCode+"%");
    
            return (Mainqueue[]) q.getResultList().toArray(new Mainqueue[0]);
        }
        finally {
            em.close();
        }
    }   

    public Mainqueue[] SelectByUserAndPool(String OrgCode,String ProcessId,String ActivityId,String UserId) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select c from Mainqueue c WHERE " +
                    "c.processId = :PID " +
                    "and c.activityId = :AID " +
                    "and (c.userId = :UID or c.userId = '') " +
                    "and c.jobId LIKE :OCODE "+
                    "ORDER BY c.jobId");
    
            q.setParameter("PID",ProcessId);
            q.setParameter("AID",ActivityId);
            q.setParameter("UID",UserId);
            q.setParameter("OCODE",OrgCode+"%");
    
            return (Mainqueue[]) q.getResultList().toArray(new Mainqueue[0]);
        }
        finally {
            em.close();
        }
    }   

}
