/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.datactrl;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Teeramanop
 */
@Entity
@Table(name = "parentqueue")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Parentqueue.findAll", query = "SELECT p FROM Parentqueue p"),
    @NamedQuery(name = "Parentqueue.findByJobId", query = "SELECT p FROM Parentqueue p WHERE p.jobId = :jobId"),
    @NamedQuery(name = "Parentqueue.findByProcessId", query = "SELECT p FROM Parentqueue p WHERE p.processId = :processId"),
    @NamedQuery(name = "Parentqueue.findByActivityId", query = "SELECT p FROM Parentqueue p WHERE p.activityId = :activityId"),
    @NamedQuery(name = "Parentqueue.findByParentProcessId", query = "SELECT p FROM Parentqueue p WHERE p.parentProcessId = :parentProcessId"),
    @NamedQuery(name = "Parentqueue.findByParentActivityId", query = "SELECT p FROM Parentqueue p WHERE p.parentActivityId = :parentActivityId"),
    @NamedQuery(name = "Parentqueue.findByVersion", query = "SELECT p FROM Parentqueue p WHERE p.version = :version")})
public class Parentqueue implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "JobId")
    private String jobId;
    @Column(name = "ProcessId")
    private String processId;
    @Column(name = "ActivityId")
    private String activityId;
    @Column(name = "ParentProcessId")
    private String parentProcessId;
    @Column(name = "ParentActivityId")
    private String parentActivityId;
    @Column(name = "Version")
    private Integer version;

    public Parentqueue() {
    }

    public Parentqueue(String jobId) {
        this.jobId = jobId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getParentProcessId() {
        return parentProcessId;
    }

    public void setParentProcessId(String parentProcessId) {
        this.parentProcessId = parentProcessId;
    }

    public String getParentActivityId() {
        return parentActivityId;
    }

    public void setParentActivityId(String parentActivityId) {
        this.parentActivityId = parentActivityId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (jobId != null ? jobId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parentqueue)) {
            return false;
        }
        Parentqueue other = (Parentqueue) object;
        if ((this.jobId == null && other.jobId != null) || (this.jobId != null && !this.jobId.equals(other.jobId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "engine.datactrl.Parentqueue[ jobId=" + jobId + " ]";
    }
    
}
