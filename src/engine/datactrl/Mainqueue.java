/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.datactrl;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Teeramanop
 */
@Entity
@Table(name = "mainqueue")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mainqueue.findAll", query = "SELECT m FROM Mainqueue m"),
    @NamedQuery(name = "Mainqueue.findByJobId", query = "SELECT m FROM Mainqueue m WHERE m.jobId = :jobId"),
    @NamedQuery(name = "Mainqueue.findByProcessId", query = "SELECT m FROM Mainqueue m WHERE m.processId = :processId"),
    @NamedQuery(name = "Mainqueue.findByActivityId", query = "SELECT m FROM Mainqueue m WHERE m.activityId = :activityId"),
    @NamedQuery(name = "Mainqueue.findBySubflow", query = "SELECT m FROM Mainqueue m WHERE m.subflow = :subflow"),
    @NamedQuery(name = "Mainqueue.findByParentProcessId", query = "SELECT m FROM Mainqueue m WHERE m.parentProcessId = :parentProcessId"),
    @NamedQuery(name = "Mainqueue.findByParentActivityId", query = "SELECT m FROM Mainqueue m WHERE m.parentActivityId = :parentActivityId"),
    @NamedQuery(name = "Mainqueue.findByHoldBy", query = "SELECT m FROM Mainqueue m WHERE m.holdBy = :holdBy"),
    @NamedQuery(name = "Mainqueue.findByHoldTime", query = "SELECT m FROM Mainqueue m WHERE m.holdTime = :holdTime"),
    @NamedQuery(name = "Mainqueue.findByUserId", query = "SELECT m FROM Mainqueue m WHERE m.userId = :userId"),
    @NamedQuery(name = "Mainqueue.findByVersion", query = "SELECT m FROM Mainqueue m WHERE m.version = :version")})
public class Mainqueue implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "JobId")
    private String jobId;
    @Column(name = "ProcessId")
    private String processId;
    @Column(name = "ActivityId")
    private String activityId;
    @Column(name = "Subflow")
    private Character subflow;
    @Column(name = "ParentProcessId")
    private String parentProcessId;
    @Column(name = "ParentActivityId")
    private String parentActivityId;
    @Column(name = "HoldBy")
    private String holdBy;
    @Column(name = "HoldTime")
    private String holdTime;
    @Column(name = "UserId")
    private String userId;
    @Column(name = "Version")
    private Integer version;

    public Mainqueue() {
    }

    public Mainqueue(String jobId) {
        this.jobId = jobId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public Character getSubflow() {
        return subflow;
    }

    public void setSubflow(Character subflow) {
        this.subflow = subflow;
    }

    public String getParentProcessId() {
        return parentProcessId;
    }

    public void setParentProcessId(String parentProcessId) {
        this.parentProcessId = parentProcessId;
    }

    public String getParentActivityId() {
        return parentActivityId;
    }

    public void setParentActivityId(String parentActivityId) {
        this.parentActivityId = parentActivityId;
    }

    public String getHoldBy() {
        return holdBy;
    }

    public void setHoldBy(String holdBy) {
        this.holdBy = holdBy;
    }

    public String getHoldTime() {
        return holdTime;
    }

    public void setHoldTime(String holdTime) {
        this.holdTime = holdTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (jobId != null ? jobId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mainqueue)) {
            return false;
        }
        Mainqueue other = (Mainqueue) object;
        if ((this.jobId == null && other.jobId != null) || (this.jobId != null && !this.jobId.equals(other.jobId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "engine.datactrl.Mainqueue[ jobId=" + jobId + " ]";
    }
    
}
