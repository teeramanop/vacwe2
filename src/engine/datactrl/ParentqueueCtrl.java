/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.datactrl;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author teeramanop
 */
public class ParentqueueCtrl {
    public ParentqueueCtrl() {
    }
    private EntityManagerFactory emf;
    private EntityManager getEntityManager() {
        if(emf == null) {
            emf = Persistence.createEntityManagerFactory("PU");
        }
        return emf.createEntityManager();
    }
    public boolean add(Parentqueue aParentqueue) {
        EntityManager em = getEntityManager();
        try{
            em.getTransaction().begin();
            // Intial version
            aParentqueue.setVersion(0);
            //
            em.persist(aParentqueue);
            em.getTransaction().commit();
        return true;
        }
    catch(Throwable theException) {
        return false;
        }
    finally {
        em.close();
        }
    }
    public boolean add(Parentqueue aParentqueue,EntityManager em) {
        try{
            if (Select(aParentqueue.getJobId(), em)!=null) {
                return false;
            }
            // Intial version
            aParentqueue.setVersion(0);
            //
            em.persist(aParentqueue);
        return true;
        }
    catch(Throwable theException) {
        return false;
        }
    finally {
        }
    }
    
    public boolean remove(Parentqueue aParentqueue) {
        EntityManager em = getEntityManager();
        try{
            em.getTransaction().begin();
            Parentqueue aParentqueuex = em.find(Parentqueue.class, aParentqueue.getJobId());
            em.remove(aParentqueuex);
            em.getTransaction().commit();
            return true;
            }
        catch(Throwable theException) {
            return false;
        }
        finally {
            em.close();
        }
    }
    public boolean remove(Parentqueue aParentqueue,EntityManager em) {
        try{
            Parentqueue aParentqueuex = em.find(Parentqueue.class, aParentqueue.getJobId());
            em.remove(aParentqueuex);
            return true;
            }
        catch(Throwable theException) {
            return false;
        }
        finally {
        }
    }

    public boolean update(Parentqueue aParentqueue) {
        EntityManager em = getEntityManager();
        try{
            em.getTransaction().begin();
            Parentqueue aParentqueuex = em.find(Parentqueue.class, aParentqueue.getJobId());
              // *** Check version
            if (!aParentqueue.getVersion().equals(aParentqueuex.getVersion())) {
                // Different verion, cannot update. (Someone already updated this reccord)
                return false;
            }
            // Increase version
            int version = aParentqueuex.getVersion();
            version++;
            if (version>100) {
                version = 0;
            }
            aParentqueuex.setVersion(version);
            // ***
            aParentqueuex.setProcessId(aParentqueue.getProcessId());
            aParentqueuex.setActivityId(aParentqueue.getActivityId());
            aParentqueuex.setParentProcessId(aParentqueue.getParentProcessId());
            aParentqueuex.setParentActivityId(aParentqueue.getParentActivityId());
            em.getTransaction().commit();
            return true;
        }
        catch(Throwable theException) {
            return false;
        }
        finally {
            em.close();
        }
    }
    public boolean update(Parentqueue aParentqueue,EntityManager em) {
        try{
            Parentqueue aParentqueuex = em.find(Parentqueue.class, aParentqueue.getJobId());
              // *** Check version
            if (!aParentqueue.getVersion().equals(aParentqueuex.getVersion())) {
                // Different verion, cannot update. (Someone already updated this reccord)
                return false;
            }
            // Increase version
            int version = aParentqueuex.getVersion();
            version++;
            if (version>100) {
                version = 0;
            }
            aParentqueuex.setVersion(version);
            // ***
            aParentqueuex.setProcessId(aParentqueue.getProcessId());
            aParentqueuex.setActivityId(aParentqueue.getActivityId());
            aParentqueuex.setParentProcessId(aParentqueue.getParentProcessId());
            aParentqueuex.setParentActivityId(aParentqueue.getParentActivityId());

            return true;
        }
        catch(Throwable theException) {
            return false;
        }
        finally {
        }
    }

    public Parentqueue Select(String JobId) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select c from Parentqueue c WHERE c.jobId = :JID");
            q.setParameter("JID",JobId);

            return (Parentqueue) q.getSingleResult();
        } 
        catch(Throwable theException) {
            return null;
        }
        finally {
            em.close();
        }
    }
    public Parentqueue Select(String JobId,EntityManager em) {
        try{
            Query q = em.createQuery("select c from Parentqueue c WHERE c.jobId = :JID");
            q.setParameter("JID",JobId);

            return (Parentqueue) q.getSingleResult();
        } 
        catch(Throwable theException) {
            return null;
        }
        finally {
        }
    }

    public Parentqueue[] SelectByJobIdProcessIdActivityId(String JobId,String ProcessId,String ActivityId) {
        EntityManager em = getEntityManager();
        try{
            Query q = em.createQuery("select c from Parentqueue c WHERE c.jobId = :JID and c.processId = :PID and c.activityId = :AID");
    
            q.setParameter("JID",JobId);
            q.setParameter("PID",ProcessId);
            q.setParameter("AID",ActivityId);
    
            return (Parentqueue[]) q.getResultList().toArray(new Parentqueue[0]);
        }
        finally {
            em.close();
        }
    }   
    public Parentqueue[] SelectByJobIdProcessIdActivityId(String JobId,String ProcessId,String ActivityId,EntityManager em) {
        try{
            Query q = em.createQuery("select c from Parentqueue c WHERE c.jobId = :JID and c.processId = :PID and c.activityId = :AID");
    
            q.setParameter("JID",JobId);
            q.setParameter("PID",ProcessId);
            q.setParameter("AID",ActivityId);
    
            return (Parentqueue[]) q.getResultList().toArray(new Parentqueue[0]);
        }
        finally {
        }
    }   
    
}
