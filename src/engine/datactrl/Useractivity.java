/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.datactrl;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Teeramanop
 */
@Entity
@Table(name = "useractivity")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Useractivity.findAll", query = "SELECT u FROM Useractivity u"),
    @NamedQuery(name = "Useractivity.findByUserId", query = "SELECT u FROM Useractivity u WHERE u.useractivityPK.userId = :userId"),
    @NamedQuery(name = "Useractivity.findByProcessId", query = "SELECT u FROM Useractivity u WHERE u.useractivityPK.processId = :processId"),
    @NamedQuery(name = "Useractivity.findByActivityId", query = "SELECT u FROM Useractivity u WHERE u.useractivityPK.activityId = :activityId"),
    @NamedQuery(name = "Useractivity.findByTimeStamp", query = "SELECT u FROM Useractivity u WHERE u.timeStamp = :timeStamp")})
public class Useractivity implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UseractivityPK useractivityPK;
    @Column(name = "TimeStamp")
    private String timeStamp;

    public Useractivity() {
    }

    public Useractivity(UseractivityPK useractivityPK) {
        this.useractivityPK = useractivityPK;
    }

    public Useractivity(String userId, String processId, String activityId) {
        this.useractivityPK = new UseractivityPK(userId, processId, activityId);
    }

    public UseractivityPK getUseractivityPK() {
        return useractivityPK;
    }

    public void setUseractivityPK(UseractivityPK useractivityPK) {
        this.useractivityPK = useractivityPK;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (useractivityPK != null ? useractivityPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Useractivity)) {
            return false;
        }
        Useractivity other = (Useractivity) object;
        if ((this.useractivityPK == null && other.useractivityPK != null) || (this.useractivityPK != null && !this.useractivityPK.equals(other.useractivityPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "engine.datactrl.Useractivity[ useractivityPK=" + useractivityPK + " ]";
    }
    
}
