/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.datactrl;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Teeramanop
 */
@Embeddable
public class UseractivityPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "UserId")
    private String userId;
    @Basic(optional = false)
    @Column(name = "ProcessId")
    private String processId;
    @Basic(optional = false)
    @Column(name = "ActivityId")
    private String activityId;

    public UseractivityPK() {
    }

    public UseractivityPK(String userId, String processId, String activityId) {
        this.userId = userId;
        this.processId = processId;
        this.activityId = activityId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        hash += (processId != null ? processId.hashCode() : 0);
        hash += (activityId != null ? activityId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UseractivityPK)) {
            return false;
        }
        UseractivityPK other = (UseractivityPK) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        if ((this.processId == null && other.processId != null) || (this.processId != null && !this.processId.equals(other.processId))) {
            return false;
        }
        if ((this.activityId == null && other.activityId != null) || (this.activityId != null && !this.activityId.equals(other.activityId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "engine.datactrl.UseractivityPK[ userId=" + userId + ", processId=" + processId + ", activityId=" + activityId + " ]";
    }
    
}
