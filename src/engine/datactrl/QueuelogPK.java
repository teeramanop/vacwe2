/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.datactrl;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Teeramanop
 */
@Embeddable
public class QueuelogPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "JobId")
    private String jobId;
    @Basic(optional = false)
    @Column(name = "StampTime")
    private String stampTime;
    @Basic(optional = false)
    @Column(name = "TrnsType")
    private String trnsType;

    public QueuelogPK() {
    }

    public QueuelogPK(String jobId, String stampTime, String trnsType) {
        this.jobId = jobId;
        this.stampTime = stampTime;
        this.trnsType = trnsType;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getStampTime() {
        return stampTime;
    }

    public void setStampTime(String stampTime) {
        this.stampTime = stampTime;
    }

    public String getTrnsType() {
        return trnsType;
    }

    public void setTrnsType(String trnsType) {
        this.trnsType = trnsType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (jobId != null ? jobId.hashCode() : 0);
        hash += (stampTime != null ? stampTime.hashCode() : 0);
        hash += (trnsType != null ? trnsType.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof QueuelogPK)) {
            return false;
        }
        QueuelogPK other = (QueuelogPK) object;
        if ((this.jobId == null && other.jobId != null) || (this.jobId != null && !this.jobId.equals(other.jobId))) {
            return false;
        }
        if ((this.stampTime == null && other.stampTime != null) || (this.stampTime != null && !this.stampTime.equals(other.stampTime))) {
            return false;
        }
        if ((this.trnsType == null && other.trnsType != null) || (this.trnsType != null && !this.trnsType.equals(other.trnsType))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "engine.datactrl.QueuelogPK[ jobId=" + jobId + ", stampTime=" + stampTime + ", trnsType=" + trnsType + " ]";
    }
    
}
