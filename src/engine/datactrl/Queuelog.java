/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.datactrl;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Teeramanop
 */
@Entity
@Table(name = "queuelog")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Queuelog.findAll", query = "SELECT q FROM Queuelog q"),
    @NamedQuery(name = "Queuelog.findByJobId", query = "SELECT q FROM Queuelog q WHERE q.queuelogPK.jobId = :jobId"),
    @NamedQuery(name = "Queuelog.findByStampTime", query = "SELECT q FROM Queuelog q WHERE q.queuelogPK.stampTime = :stampTime"),
    @NamedQuery(name = "Queuelog.findByTrnsType", query = "SELECT q FROM Queuelog q WHERE q.queuelogPK.trnsType = :trnsType"),
    @NamedQuery(name = "Queuelog.findByProcessId", query = "SELECT q FROM Queuelog q WHERE q.processId = :processId"),
    @NamedQuery(name = "Queuelog.findByActivityId", query = "SELECT q FROM Queuelog q WHERE q.activityId = :activityId"),
    @NamedQuery(name = "Queuelog.findByTrnsDate", query = "SELECT q FROM Queuelog q WHERE q.trnsDate = :trnsDate"),
    @NamedQuery(name = "Queuelog.findByTrnsTime", query = "SELECT q FROM Queuelog q WHERE q.trnsTime = :trnsTime"),
    @NamedQuery(name = "Queuelog.findByUserId", query = "SELECT q FROM Queuelog q WHERE q.userId = :userId"),
    @NamedQuery(name = "Queuelog.findByRemarks", query = "SELECT q FROM Queuelog q WHERE q.remarks = :remarks")})
public class Queuelog implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected QueuelogPK queuelogPK;
    @Column(name = "ProcessId")
    private String processId;
    @Column(name = "ActivityId")
    private String activityId;
    @Column(name = "TrnsDate")
    private String trnsDate;
    @Column(name = "TrnsTime")
    private String trnsTime;
    @Column(name = "UserId")
    private String userId;
    @Column(name = "Remarks")
    private String remarks;

    public Queuelog() {
    }

    public Queuelog(QueuelogPK queuelogPK) {
        this.queuelogPK = queuelogPK;
    }

    public Queuelog(String jobId, String stampTime, String trnsType) {
        this.queuelogPK = new QueuelogPK(jobId, stampTime, trnsType);
    }

    public QueuelogPK getQueuelogPK() {
        return queuelogPK;
    }

    public void setQueuelogPK(QueuelogPK queuelogPK) {
        this.queuelogPK = queuelogPK;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getTrnsDate() {
        return trnsDate;
    }

    public void setTrnsDate(String trnsDate) {
        this.trnsDate = trnsDate;
    }

    public String getTrnsTime() {
        return trnsTime;
    }

    public void setTrnsTime(String trnsTime) {
        this.trnsTime = trnsTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (queuelogPK != null ? queuelogPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Queuelog)) {
            return false;
        }
        Queuelog other = (Queuelog) object;
        if ((this.queuelogPK == null && other.queuelogPK != null) || (this.queuelogPK != null && !this.queuelogPK.equals(other.queuelogPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "engine.datactrl.Queuelog[ queuelogPK=" + queuelogPK + " ]";
    }
    
}
