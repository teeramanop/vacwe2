/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.datactrl;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Teeramanop
 */
public class UserroleCtrl {
    /** Creates a new instance of User1RoleCtrl */
    public UserroleCtrl() {
    }
 private EntityManagerFactory emf;
private EntityManager getEntityManager() {
if(emf == null) {
    emf = Persistence.createEntityManagerFactory("PU");
}
return emf.createEntityManager();
}

public boolean addUserRole(Userrole aUserrole) {
EntityManager em = getEntityManager();
try{
    em.getTransaction().begin();
    em.persist(aUserrole);
    em.getTransaction().commit();
    return true;
    } 
    catch(Throwable theException) {
	return false;
    }
    finally {
        em.close();
    }
}

public boolean removeUserRole(Userrole aUserRole) {
EntityManager em = getEntityManager();
try{
    em.getTransaction().begin();
    Userrole aUserRolex = em.find(Userrole.class, aUserRole.getUserrolePK());
    em.remove(aUserRolex);
    em.getTransaction().commit();
    return true;
    } 
    catch(Throwable theException) {
	return false;
    }
    finally {
        em.close();
    }
}

public boolean removeByUserID(String UserID) {
EntityManager em = getEntityManager();
try{
    em.getTransaction().begin();
    Query q = em.createQuery("delete from Userrole c WHERE c.userrolePK.userId = :UID");
    
    q.setParameter("UID",UserID);
    
    q.executeUpdate();
    em.getTransaction().commit();
    
    return true;
    }
    catch(Throwable theException) {
	return false;
    }
    finally {
    em.close();
    }
}   

public Userrole Select(UserrolePK aPK) {
EntityManager em = getEntityManager();
try{
    Query q = em.createQuery("select c from Userrole c WHERE c.userrolePK.userId = :UID and c.userrolePK.role = :ROLE");
    q.setParameter("UID",aPK.getUserId());
    q.setParameter("ROLE",aPK.getRole());

    return (Userrole) q.getSingleResult();
    } 
    catch(Throwable theException) {
    return null;
    }
    finally {
    em.close();
    }
}

public Userrole[] SelectAll() {
EntityManager em = getEntityManager();
try{
    Query q = em.createQuery("select c from Userrole c ORDER BY c.userrolePK.userId");
    return (Userrole[]) q.getResultList().toArray(new Userrole[0]);
    } finally {
    em.close();
    }
}

public Userrole[] SelectByUserID(String UserID) {
EntityManager em = getEntityManager();
try{
    Query q = em.createQuery("select c from Userrole c WHERE c.userrolePK.userId = :UID");
    
    q.setParameter("UID",UserID);
    
    return (Userrole[]) q.getResultList().toArray(new Userrole[0]);
    }
    catch(Throwable theException) {
	return null;
    }
    finally {
    em.close();
    }
}   
public Userrole[] SelectByUserID(String UserID,EntityManager em) {
try{
    Query q = em.createQuery("select c from Userrole c WHERE c.userrolePK.userId = :UID");
    
    q.setParameter("UID",UserID);
    
    return (Userrole[]) q.getResultList().toArray(new Userrole[0]);
    }
    catch(Throwable theException) {
	return null;
    }
    finally {
    }
}   
   
public Userrole[] SelectByRole(String Role) {
EntityManager em = getEntityManager();
try{
    Query q = em.createQuery("select c from Userrole c WHERE c.userrolePK.role = :RID");
    
    q.setParameter("RID",Role);
    
    return (Userrole[]) q.getResultList().toArray(new Userrole[0]);
    }
    catch(Throwable theException) {
	return null;
    }
    finally {
    em.close();
    }
}   
    
}
