/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import engine.datactrl.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import javax.persistence.EntityManager;
import xpdlparser.*;

public class Job {
	private String JobId = new String();
	private String ProcessId = new String();
	private String ActivityId = new String();
	private String HoldBy = new String();
	private String UserId = new String();

	private String s_ProcessId = new String();
	private String s_ActivityId = new String();
	private boolean IsToSubflow = false;
	private Vector WorkflowProcesses;
        
        private class Workload {
            String UserId;
            int JobCount;
            String LastStampTime;
        }

	
//    public Job(Pack pkg) {
//        WorkflowProcesses = new Vector();
//        WorkflowProcesses = pkg.getWorkflowProcesses(); 
//    }
        public Job() {
        }
    
	public java.lang.String getActivityId() {
		return ActivityId;
	}

	public java.lang.String getJobId() {
		return JobId;
	}

	public java.lang.String getProcessId() {
		return ProcessId;
	}

	public void setActivityId(java.lang.String string) {
		ActivityId = string;
	}

	public void setJobId(java.lang.String string) {
		JobId = string;
	}

	public void setProcessId(java.lang.String string) {
		ProcessId = string;
	}

        public String getHoldBy() {
            return HoldBy;
        }

        public void setHoldBy(String HoldBy) {
            this.HoldBy = HoldBy;
        }

        //2017-07-03
	public boolean addJob(String t_JobId,String t_ProcessId,String t_ActivityId,String t_UserId,EntityManager em) {
            Mainqueue aMainqueue = new Mainqueue();
            aMainqueue.setJobId(t_JobId);
            aMainqueue.setProcessId(t_ProcessId);
            aMainqueue.setActivityId(t_ActivityId);
            aMainqueue.setHoldBy("");
            aMainqueue.setHoldTime("");
            aMainqueue.setUserId(t_UserId);
            aMainqueue.setSubflow('N');
            aMainqueue.setParentProcessId("");
            aMainqueue.setParentActivityId("");
            
            MainqueueCtrl c = new MainqueueCtrl();
            if (!c.add(aMainqueue,em)) {
                return false;
            }
            
            // Add Queuelog
            Date aDate = new Date();
            QueuelogCtrl cQueuelog = new QueuelogCtrl();
            QueuelogPK kQueuelog = new QueuelogPK();
            kQueuelog.setJobId(t_JobId);
            kQueuelog.setStampTime(Long.toString(aDate.getTime()));
            kQueuelog.setTrnsType("In");
            Queuelog aQueuelog = new Queuelog();
            aQueuelog.setQueuelogPK(kQueuelog);
            aQueuelog.setProcessId(t_ProcessId);
            aQueuelog.setActivityId(t_ActivityId);
            aQueuelog.setTrnsDate(Util.DateToString(aDate, "yyyy-MM-dd"));
            aQueuelog.setTrnsTime(Util.DateToString(aDate, "HH:mm:ss"));
            aQueuelog.setUserId(t_UserId);
            aQueuelog.setRemarks("Add Job");
            if (!cQueuelog.add(aQueuelog, em)) {
                return false;
            }
            return true;
	}
        
        
        // Add Job to the first process and StartOfWorkflow activity  
	public boolean addJob(Pack pkg,String t_JobId,String t_UserId) {
            WorkflowProcesses = new Vector();
            WorkflowProcesses = pkg.getWorkflowProcesses(); 
            if (WorkflowProcesses.size()==0){
            	return false;
            }
            WorkflowProcess wfp = new WorkflowProcess();
            wfp=(WorkflowProcess)WorkflowProcesses.get(0);
		
            String t_ProcessId = wfp.getId();
            String t_ActivityId = GetStartOfWorkflow(t_ProcessId);
		
            Mainqueue aMainqueue = new Mainqueue();
            aMainqueue.setJobId(t_JobId);
            aMainqueue.setProcessId(t_ProcessId);
            aMainqueue.setActivityId(t_ActivityId);
            aMainqueue.setHoldBy("");
            aMainqueue.setHoldTime("");
            aMainqueue.setUserId(t_UserId);
            aMainqueue.setSubflow('N');
            aMainqueue.setParentProcessId("");
            aMainqueue.setParentActivityId("");
            MainqueueCtrl c = new MainqueueCtrl();
            return c.add(aMainqueue);
	}
	public boolean addJob(Pack pkg,String t_JobId,String t_UserId,EntityManager em) {
            WorkflowProcesses = new Vector();
            WorkflowProcesses = pkg.getWorkflowProcesses(); 
            if (WorkflowProcesses.size()==0){
            	return false;
            }
            WorkflowProcess wfp = new WorkflowProcess();
            wfp=(WorkflowProcess)WorkflowProcesses.get(0);
		
            String t_ProcessId = wfp.getId();
            String t_ActivityId = GetStartOfWorkflow(t_ProcessId);
		
            Mainqueue aMainqueue = new Mainqueue();
            aMainqueue.setJobId(t_JobId);
            aMainqueue.setProcessId(t_ProcessId);
            aMainqueue.setActivityId(t_ActivityId);
            aMainqueue.setHoldBy("");
            aMainqueue.setHoldTime("");
            aMainqueue.setUserId(t_UserId);
            aMainqueue.setSubflow('N');
            aMainqueue.setParentProcessId("");
            aMainqueue.setParentActivityId("");
            MainqueueCtrl c = new MainqueueCtrl();
            return c.add(aMainqueue,em);
	}
	
//	public boolean deleteJob(String JobId,int JobVersion) {
//                //Mainqueue aMainqueue = new Mainqueue();
//		//aMainqueue.setJobId(JobId);
//                MainqueueCtrl c = new MainqueueCtrl();
//                Mainqueue aMainqueue = c.Select(JobId);
//                // Check Version 2010-01-15
//                if (!aMainqueue.getVersion().equals(JobVersion)) {
//                    return false;
//                }
//                return c.remove(aMainqueue);
//	}
//	public boolean deleteJob(String JobId,int JobVersion,EntityManager em) {
//                //Mainqueue aMainqueue = new Mainqueue();
//		//aMainqueue.setJobId(JobId);
//                MainqueueCtrl c = new MainqueueCtrl();
//                Mainqueue aMainqueue = c.Select(JobId,em);
//                // Check Version 2010-01-15
//                if (!aMainqueue.getVersion().equals(JobVersion)) {
//                    return false;
//                }
//                return c.remove(aMainqueue,em);
//	}
//	
	public boolean deleteJob(String JobId) {
                //Mainqueue aMainqueue = new Mainqueue();
		//aMainqueue.setJobId(JobId);
                MainqueueCtrl c = new MainqueueCtrl();
                Mainqueue aMainqueue = c.Select(JobId);
                return c.remove(aMainqueue);
	}
	public boolean deleteJob(String JobId,EntityManager em) {
                //Mainqueue aMainqueue = new Mainqueue();
		//aMainqueue.setJobId(JobId);
                MainqueueCtrl c = new MainqueueCtrl();
                Mainqueue aMainqueue = c.Select(JobId,em);
                return c.remove(aMainqueue,em);
	}

	public String jobHoldBy(String JobId) {
                Date aDate = new Date();
                MainqueueCtrl c = new MainqueueCtrl();
                Mainqueue aMainqueue = c.Select(JobId);
                if (aMainqueue==null) {
                    return "";
                }
                if (!aMainqueue.getHoldBy().equals("")) {
                    long holdtime = Long.parseLong(aMainqueue.getHoldTime());
                    long timediff = aDate.getTime()-holdtime;
                    if (timediff < 900000 ) {
                        // 15 min = 900000 mili sec
                        return aMainqueue.getHoldBy();
                    }
                }
                return "";
	}

        public boolean holdJob(String JobId,String UserId) {
                Date aDate = new Date();
                MainqueueCtrl c = new MainqueueCtrl();
                Mainqueue aMainqueue = c.Select(JobId);
                if (aMainqueue==null) {
                    return false;
                }
                aMainqueue.setHoldBy(UserId);
                aMainqueue.setUserId(UserId);
                aMainqueue.setHoldTime(Long.toString(aDate.getTime()));
                if (!c.update(aMainqueue)) {
                    return false;
                }
                return true;
	}
	public boolean holdJob(String JobId,String UserId,EntityManager em) {
                Date aDate = new Date();
                MainqueueCtrl c = new MainqueueCtrl();
                Mainqueue aMainqueue = c.Select(JobId,em);
                if (aMainqueue==null) {
                    return false;
                }
                aMainqueue.setHoldBy(UserId);
                aMainqueue.setUserId(UserId);
                aMainqueue.setHoldTime(Long.toString(aDate.getTime()));
                if (!c.update(aMainqueue,em)) {
                    return false;
                }
                return true;
	}
	public boolean releaseJob(String JobId) {
                MainqueueCtrl c = new MainqueueCtrl();
                Mainqueue aMainqueue = c.Select(JobId);
                if (aMainqueue==null) {
                    return false;
                }
                aMainqueue.setHoldBy("");
                aMainqueue.setHoldTime("");
                aMainqueue.setUserId("");
                if (!c.update(aMainqueue)) {
                    return false;
                }
                return true;
	}
	public boolean releaseJob(String JobId,EntityManager em) {
                MainqueueCtrl c = new MainqueueCtrl();
                Mainqueue aMainqueue = c.Select(JobId,em);
                if (aMainqueue==null) {
                    return false;
                }
                aMainqueue.setHoldBy("");
                aMainqueue.setHoldTime("");
                aMainqueue.setUserId("");
                if (!c.update(aMainqueue,em)) {
                    return false;
                }
                return true;
	}
	public boolean releaseJobByUser(String UserId) {
                MainqueueCtrl c = new MainqueueCtrl();
                return c.clearHoldByUser(UserId);
	}
	public boolean releaseJobByUser(String UserId,EntityManager em) {
                MainqueueCtrl c = new MainqueueCtrl();
                return c.clearHoldByUser(UserId,em);
	}
//	public boolean releaseAllJob() {
//                MainqueueCtrl c = new MainqueueCtrl();
//                return c.clearAllHoldBy();
//	}
        

        //2017-07-03
        public boolean endJob(Pack pkg,String JobId,String Condition,String t_ProcessId,String t_ActivityId,String t_NextUserId,EntityManager em){
            WorkflowProcesses = new Vector();
            WorkflowProcesses = pkg.getWorkflowProcesses(); 
            int ii,jj; 
            int wfpSize,trnSize;
            String t_trnId;
            String t_trnName;
            String t_trnFrom;
            String t_UserId;
            String t_trnTo;
            String t_trnCondition; 
            try {
                MainqueueCtrl c = new MainqueueCtrl();
                Mainqueue aMainqueue = c.Select(JobId,em);
                aMainqueue.setHoldBy("");
                t_UserId = aMainqueue.getUserId();
//			
//                // Check Version 2010-01-15
//                if (!aMainqueue.getVersion().equals(JobVersion)) {
//                    return false;
//                }
                ProcessId = aMainqueue.getProcessId();
		ActivityId = aMainqueue.getActivityId();
//
                if (!ProcessId.equals(t_ProcessId)) {
                    return false;
                }
                if (!ActivityId.equals(t_ActivityId)) {
                    return false;
                }

                wfpSize = WorkflowProcesses.size();
		t_trnId = "";
                boolean conditionfound = false;
		ii = 0;
		while(ii < wfpSize){
                    WorkflowProcess wfp = new WorkflowProcess();
                    wfp=(WorkflowProcess)WorkflowProcesses.get(ii);
                    if (wfp.getId().equals(ProcessId)) {
                        trnSize = wfp.getTransitions().size();
			jj = 0;
			while(jj < trnSize){
                            Transition trn = new Transition();
                            trn=(Transition)wfp.getTransitions().get(jj);
                            if (trn.getFrom().equals(ActivityId)){
                               t_trnId = trn.getId();
                               t_trnName = trn.getName();
                               t_trnFrom = trn.getFrom();
                               t_trnTo = trn.getTo();
                               t_trnCondition = trn.getCondition(); 	

                               if (t_trnCondition.equals(Condition)){
                                   conditionfound = true;
                                   if (toSubflow(wfp,t_trnTo)) {
                                      //* To SubFlow Activity
                                      Parentqueue aParentqueue = new Parentqueue();
	                              aParentqueue.setJobId(aMainqueue.getJobId());
                                      aParentqueue.setProcessId(aMainqueue.getProcessId());
                                      aParentqueue.setActivityId(t_trnTo);
                                      aParentqueue.setParentProcessId(aMainqueue.getParentProcessId());
                                      aParentqueue.setParentActivityId(aMainqueue.getParentActivityId());
                                      ParentqueueCtrl pc = new ParentqueueCtrl();
                                      if (!pc.add(aParentqueue,em)) {
                                  	  return false;
                                      }
	                              aMainqueue.setParentProcessId(aParentqueue.getProcessId());
                                      aMainqueue.setParentActivityId(t_trnTo);
                                      aMainqueue.setProcessId(s_ProcessId);
                                      aMainqueue.setActivityId(s_ActivityId);
				      aMainqueue.setSubflow('Y');
                                      if (!t_NextUserId.equals("")) {
                                          aMainqueue.setUserId(t_NextUserId);
                                      } else {
                                          t_NextUserId = GetNextUser(ProcessId, t_trnTo);
                                          aMainqueue.setUserId(t_NextUserId);
                                      }
                                      if (!c.update(aMainqueue,em)) {
            				  return false;
				      }
                                   } else {
                                       // * Update MainQueue
                                       aMainqueue.setProcessId(ProcessId);
                                       aMainqueue.setActivityId(t_trnTo);
                                       if (!t_NextUserId.equals("")) {
                                           aMainqueue.setUserId(t_NextUserId);
                                       } else {
                                         t_NextUserId = GetNextUser(ProcessId, t_trnTo);
                                         aMainqueue.setUserId(t_NextUserId);
                                       }
                                       if (!c.update(aMainqueue,em)) {
                                        return false;
                                       }
                                   }
                                   
                                   Date aDate = new Date();
                                   if (!t_NextUserId.equals("")) {
                                       // Update UserActivity
                                       UseractivityCtrl cUseractivity = new UseractivityCtrl();
                                       UseractivityPK kUseractivity = new UseractivityPK();
                                       kUseractivity.setUserId(t_NextUserId);
                                       kUseractivity.setProcessId(ProcessId);
                                       kUseractivity.setActivityId(t_trnTo);
                                       Useractivity aUseractivity = cUseractivity.Select(kUseractivity,em);
                                       if (aUseractivity==null) {
                                           // add
                                           aUseractivity = new Useractivity();
                                           aUseractivity.setUseractivityPK(kUseractivity);
                                           aUseractivity.setTimeStamp(Long.toString(aDate.getTime()));
                                           if (!cUseractivity.add(aUseractivity,em)) {
                                               return false;
                                           }
                                        } else {
                                           // update timestamp
                                           aUseractivity.setTimeStamp(Long.toString(aDate.getTime()));
                                           if (!cUseractivity.update(aUseractivity,em)) {
                                               return false;
                                           }
                                        }
                                     }
                                    // Add QueueLog Out
                                    aDate = new Date();
                                    QueuelogCtrl cQueueLog = new QueuelogCtrl();
                                    QueuelogPK kQueueLog = new QueuelogPK();
                                    kQueueLog.setJobId(JobId);
                                    kQueueLog.setStampTime(Long.toString(aDate.getTime()));
                                    kQueueLog.setTrnsType("Out");
                                    Queuelog aQueueLog = new Queuelog();
                                    aQueueLog.setQueuelogPK(kQueueLog);
                                    aQueueLog.setProcessId(t_ProcessId);
                                    aQueueLog.setActivityId(t_ActivityId);
                                    aQueueLog.setTrnsDate(Util.DateToString(aDate, "yyyy-MM-dd"));
                                    aQueueLog.setTrnsTime(Util.DateToString(aDate, "HH:mm:ss"));
                                    aQueueLog.setUserId(t_UserId);
                                    aQueueLog.setRemarks("To: "+ t_trnTo);
                                    if (!cQueueLog.add(aQueueLog,em)) {
                                        return false;
                                    }
                                    // Add QueueLog In
                                    aDate = new Date();
                                    kQueueLog = new QueuelogPK();
                                    kQueueLog.setJobId(JobId);
                                    kQueueLog.setStampTime(Long.toString(aDate.getTime()));
                                    kQueueLog.setTrnsType("In");
                                    aQueueLog = new Queuelog();
                                    aQueueLog.setQueuelogPK(kQueueLog);
                                    aQueueLog.setProcessId(t_ProcessId);
                                    aQueueLog.setActivityId(t_trnTo);
                                    aQueueLog.setTrnsDate(Util.DateToString(aDate, "yyyy-MM-dd"));
                                    aQueueLog.setTrnsTime(Util.DateToString(aDate, "HH:mm:ss"));
                                    aQueueLog.setUserId(t_NextUserId);
                                    aQueueLog.setRemarks("From: "+ t_ActivityId);
                                    if (!cQueueLog.add(aQueueLog,em)) {
                                        return false;
                                    }
                                    return true;	
				}
                            }
                            jj++;
                        }
                    }
                    ii++;  
		}
                
		if (t_trnId.equals("")) {
                    if (aMainqueue.getSubflow().equals('Y')) {
                    //*	Select Transitions From=ParentActivityId in ParentProcess
                        conditionfound = true;
			ii = 0;
			while(ii < wfpSize){
                            WorkflowProcess wfp = new WorkflowProcess();
                            wfp=(WorkflowProcess)WorkflowProcesses.get(ii);
                            if (wfp.getId().equals(aMainqueue.getParentProcessId())) {
                                trnSize = wfp.getTransitions().size();
				jj = 0;
				while(jj < trnSize){
                                    Transition trn = new Transition();
                                    trn=(Transition)wfp.getTransitions().get(jj);
                                    if (trn.getFrom().equals(aMainqueue.getParentActivityId())){
                                        if (trn.getCondition().equals(Condition)){
                                            ParentqueueCtrl pc = new ParentqueueCtrl();
                                            Parentqueue[] aParentqueues = pc.SelectByJobIdProcessIdActivityId(aMainqueue.getJobId(), aMainqueue.getParentProcessId(),aMainqueue.getParentActivityId(),em);
                                            Parentqueue aParentqueue = aParentqueues[0];
						   	
					    aMainqueue.setProcessId(aParentqueue.getProcessId());
			                    aMainqueue.setActivityId(trn.getTo());
                                            aMainqueue.setParentProcessId(aParentqueue.getParentProcessId());
                                            aMainqueue.setParentActivityId(aParentqueue.getParentActivityId());
                                            if (aParentqueue.getParentActivityId().equals("")) {
						aMainqueue.setSubflow('N');
                                            }
                                            if (!t_NextUserId.equals("")) {
                                                aMainqueue.setUserId(t_NextUserId);
                                            } else {
                                                t_NextUserId = GetNextUser(aMainqueue.getProcessId(), aMainqueue.getActivityId());
                                                aMainqueue.setUserId(t_NextUserId);
                                            }
                                            if (!c.update(aMainqueue,em)) {
                                                return false;
                                            }
			                    if (!pc.remove(aParentqueue,em)) {
                                                return false;
                                            }
                                         }
                                    }
                                    jj++;
                                }
                            }
                            ii++;  
                        }
                    }
               	    else {
                        //aMainqueue.setActivityId("EndofWorkflow");
                        //c.update(aMainqueue,em);
                    }
                }
                
                if (!conditionfound) {
                    return false;
                }
                return true;
		}
		catch(Exception exc){
			exc.printStackTrace();
			return false;
		}
	}
        
        public boolean toSubflow(WorkflowProcess wfp, String ActivityId){
		int ii,jj,actSize,sfSize;
		
		try {
                    IsToSubflow = false;
                    actSize = wfp.getActivities().size();
                    ii = 0;
                    while(ii < actSize){
			Activity act = new Activity();
			act=(Activity)wfp.getActivities().get(ii);
                        if (act.getId().equals(ActivityId)) {
                            sfSize = act.getSubflows().size();
                            jj = 0;
                            while(jj < sfSize){
                            	IsToSubflow = true;
				Subflow sf = new Subflow();
				sf = (Subflow)act.getSubflows().get(jj);
				s_ProcessId = sf.getId();
				s_ActivityId = GetStartOfWorkflow(s_ProcessId);
				jj++;
                            }
                        }
			ii++;
                    }
                    return IsToSubflow;
                }
		catch(Exception exc){
                    exc.printStackTrace();
                    return false;
		}
	}
        //2017-07-03
        private String GetNextUser(String t_ProcessId,String t_ActivityId) {
            
            // getActivity
            Activity aActivity = new Activity();
            aActivity = GetActivity(t_ProcessId, t_ActivityId);
            if (aActivity==null) {
                return "";
            }
            if (aActivity.getPriority()==null) {
                aActivity.setPriority("Pool");
            }
            // Check how to assign job  (Priority = Assign,Pool,Workload,Round)
            if (aActivity.getPriority().equals("Pool")) {
                return "";
            }
            if (aActivity.getPriority().equals("Workload")) {
                return UserbyWorkload(t_ProcessId, t_ActivityId, aActivity.getPerformer());
            }
            if (aActivity.getPriority().equals("Round")) {
                return UserbyRound(t_ProcessId, t_ActivityId, aActivity.getPerformer());
            }
            
            
            return "";
        }
        
        private Activity GetActivity(String t_ProcessId,String t_ActivityId) {
            Activity act = new Activity();
            int ii = 0;
            while(ii < WorkflowProcesses.size()){
                WorkflowProcess wfp = new WorkflowProcess();
                wfp=(WorkflowProcess)WorkflowProcesses.get(ii);
                if (wfp.getId().equals(t_ProcessId)) {
                    int jj = 0;
                    while(jj < wfp.getActivities().size()) {
                        act=(Activity)wfp.getActivities().get(jj);
                        if (act.getId().equals(t_ActivityId)) {
                            return act;
                        }
                        jj++;
                    }
                }
                ii++;
            }
            return null;
        }
        
        public String SelectPerformer(Pack pkg,String t_ProcessId,String t_ActivityId) {
            WorkflowProcesses = new Vector();
            WorkflowProcesses = pkg.getWorkflowProcesses(); 
            Activity act = new Activity();
            int ii = 0;
            while(ii < WorkflowProcesses.size()){
                WorkflowProcess wfp = new WorkflowProcess();
                wfp=(WorkflowProcess)WorkflowProcesses.get(ii);
                if (wfp.getId().equals(t_ProcessId)) {
                    int jj = 0;
                    while(jj < wfp.getActivities().size()) {
                        act=(Activity)wfp.getActivities().get(jj);
                        if (act.getId().equals(t_ActivityId)) {
                            return act.getPerformer();
                        }
                        jj++;
                    }
                }
                ii++;
            }
            return null;
        }

        private String UserbyRound(String t_ProcessId,String t_ActivityId,String role) {
            UserroleCtrl cUserRole = new UserroleCtrl();
            Userrole[] aUserRoles = cUserRole.SelectByRole(role);

            UseractivityCtrl cUseractivity = new UseractivityCtrl();
            

            // select user for First Time in Roles
            int ii = 0;
            while (ii<aUserRoles.length) {
                UseractivityPK kUseractivity = new UseractivityPK();
                kUseractivity.setUserId(aUserRoles[ii].getUserrolePK().getUserId());
                kUseractivity.setProcessId(t_ProcessId);
                kUseractivity.setActivityId(t_ActivityId);
                Useractivity aUseractivity = cUseractivity.Select(kUseractivity);
                if (aUseractivity==null) {
                    return aUserRoles[ii].getUserrolePK().getUserId();
                }
                ii++;
            }
            
            // if no first time user --> select lowest timestamp
            Useractivity[] aUseractivitys = cUseractivity.SelectByActivity(t_ProcessId, t_ActivityId);
            if (aUseractivitys.length>0) {
                return aUseractivitys[0].getUseractivityPK().getUserId();
            }
            
            return "";
        }
        private String UserbyWorkload(String t_ProcessId,String t_ActivityId,String role) {
            UserroleCtrl cUserRole = new UserroleCtrl();
            Userrole[] aUserRoles = cUserRole.SelectByRole(role);
            
            MainqueueCtrl cMainqueue = new MainqueueCtrl();
            
            UseractivityCtrl cUseractivity = new UseractivityCtrl();
            
            ArrayList<Workload> aWorkloads = new ArrayList<>();
            
            int ii = 0;
            while (ii<aUserRoles.length) {
                Workload aWorkload = new Workload();
                aWorkload.UserId=aUserRoles[ii].getUserrolePK().getUserId();
                //
                Mainqueue[] aMainqueues = cMainqueue.SelectByUser(t_ProcessId, t_ActivityId, aUserRoles[ii].getUserrolePK().getUserId());
                aWorkload.JobCount=aMainqueues.length;
                // 
                UseractivityPK kUseractivity = new UseractivityPK();
                kUseractivity.setUserId(aUserRoles[ii].getUserrolePK().getUserId());
                kUseractivity.setProcessId(t_ProcessId);
                kUseractivity.setActivityId(t_ActivityId);
                Useractivity aUseractivity = cUseractivity.Select(kUseractivity);
                if (aUseractivity==null) {
                    aWorkload.LastStampTime = "";
                } else {
                    aWorkload.LastStampTime = aUseractivity.getTimeStamp();
                }
                aWorkloads.add(aWorkload);
                ii++;
            }
            Workload aWorkload = new Workload();
            // Sort by JobCount and TimeStamp
            String key1;
            String key2;
            int runlen;        
            ii=0;
            int jj=0;
            while (ii<aWorkloads.size()-1) {
                jj=ii+1;
                while (jj<aWorkloads.size()) {
                    key1 = "00000" + Integer.toString(aWorkloads.get(ii).JobCount);
                    runlen = key1.length();
                    key1 = key1.substring(runlen-5,runlen);
                    key1 = key1+aWorkloads.get(ii).LastStampTime;
                    
                    key2 = "00000" + Integer.toString(aWorkloads.get(jj).JobCount);
                    runlen = key2.length();
                    key2 = key2.substring(runlen-5,runlen);
                    key2 = key2+aWorkloads.get(jj).LastStampTime;
                    
                    if (key1.compareTo(key2)>0) {
                        aWorkload = aWorkloads.get(ii);
                        aWorkloads.set(ii, aWorkloads.get(jj));
                        aWorkloads.set(jj, aWorkload);
                    }
                    jj++;
                }
                ii++;
            }
            if (aWorkloads.size()>0) {
                return aWorkloads.get(0).UserId;
            }
            return "";
        }

        public void CheckSubflow(WorkflowProcess wfp, String ActivityId){
		int ii,jj,actSize,sfSize;
		
		try {
                    IsToSubflow = false;
                    actSize = wfp.getActivities().size();
                    ii = 0;
                    while(ii < actSize){
			Activity act = new Activity();
			act=(Activity)wfp.getActivities().get(ii);
                        if (act.getId().equals(ActivityId)) {
                            sfSize = act.getSubflows().size();
                            jj = 0;
                            while(jj < sfSize){
                            	IsToSubflow = true;
				Subflow sf = new Subflow();
				sf = (Subflow)act.getSubflows().get(jj);
				s_ProcessId = sf.getId();
				s_ActivityId = GetStartOfWorkflow(s_ProcessId);
				jj++;
                            }
                        }
			ii++;
                    }
                }
		catch(Exception exc){
                    exc.printStackTrace();
                    return;
		}
	}
   
    //* Return Start ActivityId  in the Process (ExtendedAttribute) 
	private String GetStartOfWorkflow(String ProcessId) {

 		int i,ii,jj,wfpSize,extSize,actSize;
 		String t1,t2;
		try {
                    wfpSize = WorkflowProcesses.size();
                    ii = 0;
                    while(ii < wfpSize){
			WorkflowProcess wfp = new WorkflowProcess();
			wfp=(WorkflowProcess)WorkflowProcesses.get(ii);
			if (wfp.getId().equals(ProcessId)) {

// .xpdl new version cannot set ExtendedAttributes
//                        extSize = wfp.getExtendedAttributes().size();
//                        jj=0;
//			while(jj < extSize){
//                           ExtendedAttribute ext = new ExtendedAttribute();
//                    	   ext=(ExtendedAttribute)wfp.getExtendedAttributes().get(jj);
//		   	   if (ext.getName().equals("StartOfWorkflow")) {
//                                 i = ext.getValue().indexOf(";");
//                                i++;
//                                t1 = ext.getValue().substring(i);
//                                i = t1.indexOf(";");
//                                t2 = t1.substring(0,i);
//                                return t2;
//                           }
//                           jj++;
//                        }
                          actSize = wfp.getActivities().size();
                          if (actSize>0) {
                             Activity act = new Activity();
                             act=(Activity)wfp.getActivities().get(0);
                             t2 = act.getId();
                             return t2;
                          }
                      }
                      ii++;
                    }
                    return "";
		}
		catch(Exception exc){
			exc.printStackTrace();
			return "";
		}
   }
   
   public Mainqueue[] SelectJobs(String ProcessId,String ActivityId) {
	   try {
                MainqueueCtrl c = new MainqueueCtrl();
   		Mainqueue[] aMainqueues = c.SelectByProcessIdActivityId(ProcessId, ActivityId);
	   	return aMainqueues;
	   }   
	   catch(Exception exc){
		   exc.printStackTrace();
		   return null;
	   }
   	
   }
   public Mainqueue[] SelectJobs(String ProcessId,String ActivityId,EntityManager em) {
	   try {
                MainqueueCtrl c = new MainqueueCtrl();
   		Mainqueue[] aMainqueues = c.SelectByProcessIdActivityId(ProcessId, ActivityId,em);
	   	return aMainqueues;
	   }   
	   catch(Exception exc){
		   exc.printStackTrace();
		   return null;
	   }
   	
   }
    
   public Mainqueue[] SelectJobs(String OrgCode,String ProcessId,String ActivityId) {
	   try {
                MainqueueCtrl c = new MainqueueCtrl();
   		Mainqueue[] aMainqueues = c.SelectByOrgCodeProcessIdActivityId(OrgCode,ProcessId, ActivityId);
	   	return aMainqueues;
	   }   
	   catch(Exception exc){
		   exc.printStackTrace();
		   return null;
	   }
   	
   }
   public Mainqueue[] SelectJobs(String OrgCode,String ProcessId,String ActivityId,EntityManager em) {
	   try {
                MainqueueCtrl c = new MainqueueCtrl();
   		Mainqueue[] aMainqueues = c.SelectByOrgCodeProcessIdActivityId(OrgCode,ProcessId, ActivityId,em);
	   	return aMainqueues;
	   }   
	   catch(Exception exc){
		   exc.printStackTrace();
		   return null;
	   }
   	
   }
   
   public Mainqueue[] SelectByUserAndPool(String OrgCode,String ProcessId,String ActivityId,String UserId) {
	   try {
                MainqueueCtrl c = new MainqueueCtrl();
   		Mainqueue[] aMainqueues = c.SelectByUserAndPool(OrgCode,ProcessId, ActivityId,UserId );
	   	return aMainqueues;
	   }   
	   catch(Exception exc){
		   exc.printStackTrace();
		   return null;
	   }
   	
   }
   
   public Mainqueue[] SelectByUser(String OrgCode,String ProcessId,String ActivityId,String UserId) {
	   try {
                MainqueueCtrl c = new MainqueueCtrl();
   		Mainqueue[] aMainqueues = c.SelectByUser(OrgCode,ProcessId, ActivityId,UserId );
	   	return aMainqueues;
	   }   
	   catch(Exception exc){
		   exc.printStackTrace();
		   return null;
	   }
   	
   }
   
   public Mainqueue[] SelectFromPool(String OrgCode,String ProcessId,String ActivityId) {
	   try {
                MainqueueCtrl c = new MainqueueCtrl();
   		Mainqueue[] aMainqueues = c.SelectFromPool(OrgCode,ProcessId, ActivityId );
	   	return aMainqueues;
	   }   
	   catch(Exception exc){
		   exc.printStackTrace();
		   return null;
	   }
   	
   }
}
