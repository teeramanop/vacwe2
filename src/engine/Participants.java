/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package engine;

import java.util.Vector;
import xpdlparser.*;

/**
 *
 * @author teeramanop
 */
public class Participants {
	private java.lang.String[] Id = null;
	private java.lang.String[] Name = null;

	private java.lang.String SQL = new String();

	private java.lang.String RetCode = new String();
	private java.lang.String RetDesc = new String();
	private int Reccount;

	public int getReccount() {
		return Reccount;
	}

	public java.lang.String getRetCode() {
		return RetCode;
	}

	public java.lang.String getRetDesc() {
		return RetDesc;
	}

	public java.lang.String[] getId() {
		return Id;
	}
	public java.lang.String getId(int index) {
		return getId()[index];
	}

	public java.lang.String[] getName() {
		return Name;
	}
	public java.lang.String getName(int index) {
		return getName()[index];
	}

	public void setReccount(int i) {
		Reccount = i;
	}

	public void setRetCode(java.lang.String string) {
		RetCode = string;
	}

	public void setRetDesc(java.lang.String string) {
		RetDesc = string;
	}

	public void setId(java.lang.String[] strings) {
		Id = strings;
	}
	public void setId(int index, java.lang.String id) {
		Id[index] = id;
	}

	public void setName(java.lang.String[] strings) {
		Name = strings;
	}
	public void setName(int index, java.lang.String name) {
		Name[index] = name;
	}


	public void selectParticipants(Pack pkg) {
	int ii;
	int ptpSize;
	Vector t_ParticipantId = new Vector();
	Vector t_ParticipantName = new Vector();
	try {
		ptpSize = pkg.getParticipants().size();
		ii = 0;
		while(ii < ptpSize){
			Participant ptp = new Participant();
			ptp =(Participant)pkg.getParticipants().get(ii);
			t_ParticipantId.addElement(ptp.getId());			
    		t_ParticipantName.addElement(ptp.getName());			
            ii++;  
			}
		Reccount = ii;
		if (ii != 0) {
			Id = new String[ii];
			Name = new String[ii];
			}
		ii = 0;
		while(ii<Reccount){
			Id[ii] = (String)t_ParticipantId.get(ii);
			Name[ii] = (String)t_ParticipantName.get(ii);
			ii++;
			}
		RetCode = "0";
		RetDesc = "Completed";
		return;

	}
	catch(Exception exc){
		exc.printStackTrace();
		return;
	}
		
	}
    
}
